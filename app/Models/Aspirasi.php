<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Aspirasi extends Model
{
    use HasFactory;
    protected $table = 'aspirasis';
    protected $primaryKey = 'id_aspirasi';
    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'id_aspirasi',
        'email',
        'nomor_handphone',
        'aspirasi',
        'created_at'
    ];

    public $incrementing = false;

    protected $casts = [
        'id_aspirasi'=>'string'
    ];
}
