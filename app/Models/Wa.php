<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Wa extends Model
{
    use HasFactory;
    protected $table = 'was';
    protected $primaryKey = 'nomor';
    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'nomor',
        'nama_lengkap',
        'sumber_data',
        'status',
        'response',
        'created_at',
        'updated_at'
    ];
}
