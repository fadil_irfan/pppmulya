<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MasterPengeluaran extends Model
{
    use HasFactory;
    protected $primaryKey = 'id_pengeluaran';
    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'id_pengeluaran',
        'kode',
        'nama_pengeluaran',
        'penjelasan',
    ];

    public $incrementing = false;

    protected $casts = [
        'id_pengeluaran'=>'string'
    ];
}
