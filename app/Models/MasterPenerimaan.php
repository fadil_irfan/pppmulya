<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MasterPenerimaan extends Model
{
    use HasFactory;
    protected $primaryKey = 'id_penerimaan';
    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'id_penerimaan',
        'kode',
        'nama_pemasukan',
        'penjelasan',
    ];

    public $incrementing = false;

    protected $casts = [
        'id_penerimaan'=>'string'
    ];

}
