<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TransaksiPenerimaan extends Model
{
    use HasFactory;
    protected $primaryKey = 'id_transaksi_penerimaan';
    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'id_transaksi_penerimaan',
        'kode_penerimaan',
        'no_transaksi_masuk',
        'deskripsi_penerimaan',
        'jumlah_penerimaan',
        'kode_unit',
        'validasi',
        'eviden',
        'status',
    ];

    public $incrementing = false;

    protected $casts = [
        'id_transaksi_penerimaan'=>'string'
    ];


}
