<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TransaksiPengeluaran extends Model
{
    use HasFactory;
    protected $primaryKey = 'id_transaksi_pengeluaran';
    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'id_transaksi_pengeluaran',
        'kode_pengeluaran',
        'no_transaksi_keluar',
        'deskripsi_pengeluaran',
        'jumlah_pengeluaran',
        'kode_unit',
        'validasi',
        'eviden',
        'status',
    ];

    public $incrementing = false;

    protected $casts = [
        'id_transaksi_pengeluaran'=>'string'
    ];
}
