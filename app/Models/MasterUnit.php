<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MasterUnit extends Model
{
    use HasFactory;
    protected $table = 'master_units';
    protected $primaryKey = 'id_unit';
    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'id_unit',
        'kode_unit',
        'nama_unit',
        'alamat_unit',
        'jabatan_tanda_wadek',
        'nama_tanda_wadek',
        'jabatan_tanda_kepala',
        'nama_tanda_kepala',
        'kop_unit'
    ];

    public $incrementing = false;

    protected $casts = [
        'id_unit'=>'string'
    ];
}
