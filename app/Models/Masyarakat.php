<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Masyarakat extends Model
{
    use HasFactory;
    protected $table = 'masyarakats';
    protected $primaryKey = 'id_masyarakat';
    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'id_masyarakat',
        'nik',
        'nama_lengkap',
        'nomor_wa',
        'kabupaten',
        'kecamatan',
        'kelurahan',
        'rt',
        'rw',
        'koordinator'
    ];

    public $incrementing = false;

    protected $casts = [
        'id_masyarakat'=>'string'
    ];
}
