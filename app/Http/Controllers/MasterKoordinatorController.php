<?php

namespace App\Http\Controllers;

use App\Models\MasterPenerimaan;
use App\Models\Masyarakat;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Ramsey\Uuid\Uuid;
use RealRashid\SweetAlert\Facades\Alert;
use Yajra\DataTables\DataTables;


class MasterKoordinatorController extends Controller
{
    public function index(Request $request)
    {

        if ($request->ajax()) {
            $data = Masyarakat::select(DB::raw('id_masyarakat,nik,nama_lengkap,nomor_wa,concat(nama_kab,", ",nama_kec,", ",nama_kel) as alamat'))
            ->join('wilayah_kelurahan','wilayah_kelurahan.kode_kel','masyarakats.kelurahan')
            ->where('koordinator','=','1')
            ->orderby('created_at','desc')
            ->get();
            return DataTables::of($data)
                    ->addIndexColumn()
                    ->addColumn('action', function($row){

                           $btn = '<a href="javascript:void(0)" data-toggle="tooltip"  data-id="'.$row->id_masyarakat.'" data-original-title="Edit" class="edit btn btn-primary btn-sm editCustomer"><i class="fas fa-edit"></i>Edit</a>';

                           $btn = $btn.' <a href="javascript:void(0)" data-toggle="modal" data-target="#modal-delete" data-id="'.$row->id_masyarakat.'" data-original-title="Delete" class="btn btn-danger btn-sm deleteCustomer"><i class="fa fa-trash" aria-hidden="true"></i> Delete</a>';

                            return $btn;
                    })
                    ->rawColumns(['action'])
                    ->make(true);
        }
        return view('dashboard.koordinator.index');
    }

    public function store(Request $request)
    {
        if ($request->idPenerimaan=='')
        {
            MasterPenerimaan::create([
                'id_penerimaan' => Uuid::uuid4()->toString(),
                'kode' => $request->kode,'nama_pemasukan' => $request->nama_pemasukan,'penjelasan' => $request->penjelasan
                ]);
        }else
        {
            MasterPenerimaan::updateOrCreate(
                ['id_penerimaan' => $request->idPenerimaan],
                ['kode' => $request->kode,'nama_pemasukan' => $request->nama_pemasukan,'penjelasan' => $request->penjelasan]
            );
        }


        return response()->json(['success'=>'Data Sukses di Simpan']);
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Customer  $Customer
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $penerimaan = MasterPenerimaan::find($id);
        return response()->json($penerimaan);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Customer  $Customer
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        MasterPenerimaan::find($id)->delete();

        return response()->json(['success'=>'Data Penerimaan Sukses Dihapus !']);
    }

    public function dashboard()
    {
        
        $unit="";
        $relawan=Masyarakat::where('koordinator','=','1')
        ->count();
        $masyarakat=Masyarakat::where('koordinator','<>','1')
        ->count();
        $wilayah=Masyarakat::select(DB::raw('kabupaten,kecamatan,kelurahan'))->get();
        $total=intval($relawan)+intval($masyarakat);
        return view('dashboard.homekoordinator.index',[
            'unit'=>$unit,
            'relawan'=>$relawan,
            'masyarakat'=>$masyarakat,
            'total'=>$total,
            'wilayah'=>$wilayah
        ]);
    }

    public function masyarakat(Request $request)
    {

        if ($request->ajax()) {
            $data = Masyarakat::select(DB::raw('id_masyarakat,nik,nama_lengkap,nomor_wa,concat(nama_kab,", ",nama_kec,", ",nama_kel) as alamat,koordinator'))
            ->join('wilayah_kelurahan','wilayah_kelurahan.kode_kel','masyarakats.kelurahan')
            ->where('koordinator','<>','1')
            ->orderby('created_at','desc')
            ->get();
            return DataTables::of($data)
                    ->addIndexColumn()
                    ->addColumn('action', function($row){

                           $btn = '<a href="javascript:void(0)" data-toggle="tooltip"  data-id="'.$row->id_masyarakat.'" data-original-title="Edit" class="edit btn btn-primary btn-sm editCustomer"><i class="fas fa-edit"></i>Edit</a>';

                           $btn = $btn.' <a href="javascript:void(0)" data-toggle="modal" data-target="#modal-delete" data-id="'.$row->id_masyarakat.'" data-original-title="Delete" class="btn btn-danger btn-sm deleteCustomer"><i class="fa fa-trash" aria-hidden="true"></i> Delete</a>';

                            return $btn;
                    })
                    ->rawColumns(['action'])
                    ->make(true);
        }
        return view('dashboard.homekoordinator.masyarakat');
    }

    public function tambahmasyarakat()
    {
        $kecamatan=DB::table('wilayah_kelurahan')->select('kode_kec', 'nama_kec')
        ->distinct()
        ->orderby('kode_kec')
        ->get();

        $kabupaten=DB::table('wilayah_kelurahan')->select('kode_kab', 'nama_kab')
        ->distinct()
        ->orderby('kode_kab')
        ->get();

        $kelurahan=DB::table('wilayah_kelurahan')->select('kode_kel', 'nama_kel')
        ->orderby('kode_kec')
        ->orderby('kode_kel')
        ->get();

        return view('dashboard.homekoordinator.tambahmasyarakat',[
            'kecamatan'=>$kecamatan,
            'kelurahan'=>$kelurahan,
            'kabupaten'=>$kabupaten
        ]);
    }

    public function rekapmasyarakat()
    {
        $kecamatan=DB::table('wilayah_kelurahan')->select('kode_kec', 'nama_kec')
        ->distinct()
        ->orderby('kode_kec')
        ->get();

        $kabupaten=DB::table('wilayah_kelurahan')->select('kode_kab', 'nama_kab')
        ->distinct()
        ->orderby('kode_kab')
        ->get();

        $kelurahan=DB::table('wilayah_kelurahan')->select('kode_kel', 'nama_kel')
        ->orderby('kode_kec')
        ->orderby('kode_kel')
        ->get();
        return view('dashboard.homekoordinator.form_masyarakat',[
            'kecamatan'=>$kecamatan,
            'kelurahan'=>$kelurahan,
            'kabupaten'=>$kabupaten
        ]);
    }

    public function storeKoordinator(Request $request)
    {
        $request->validate([
            'nik' => 'required|min:16|max:16',
            'nama_lengkap' => 'required',
            'nomor_wa' => 'required|starts_with:0|min:10',
            'kabupaten' => 'required',
            'kecamatan' => 'required',
            'kelurahan' => 'required',
            'rt' => 'required',
            'rw' => 'required',
        ]);

        try 
        {
            $id=Uuid::uuid4()->toString();
            $now = \Carbon\Carbon::now()->toDateTimeString(); 
                DB::table('masyarakats')->insert([
                    'id_masyarakat' => $id,
                    'nik' => $request->nik,
                    'nama_lengkap' => $request->nama_lengkap,
                    'nomor_wa' => $request->nomor_wa,
                    'kecamatan' => $request->kecamatan,
                    'kabupaten' => $request->kabupaten,
                    'kelurahan' => $request->kelurahan,
                    'rt' => $request->rt,
                    'rw' => $request->rw,
                    'created_at'=>$now,
                    'koordinator' => session('id_masyarakat'),
                ]);  
                
            return response()->json(['status'=>'200','success'=>'Data Berhasil Masuk']);

        } catch (Exception $e) {
            return response()->json(['status'=>'201','error'=>$e->getMessage()]);
            
        }

    }

}
