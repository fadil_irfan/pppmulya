<?php

namespace App\Http\Controllers;

use App\Models\MasterUnit;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Barryvdh\DomPDF\Facade\Pdf;
use Illuminate\Support\Facades\DB;

class RekapKoordinatorController extends Controller
{
    public function index(Request $request)
    {
        $kecamatan=DB::table('wilayah_kelurahan')->select('kode_kec', 'nama_kec')
        ->distinct()
        ->orderby('kode_kec')
        ->get();

        $kabupaten=DB::table('wilayah_kelurahan')->select('kode_kab', 'nama_kab')
        ->distinct()
        ->orderby('kode_kab')
        ->get();

        $kelurahan=DB::table('wilayah_kelurahan')->select('kode_kel', 'nama_kel')
        ->orderby('kode_kec')
        ->orderby('kode_kel')
        ->get();
        return view('dashboard.rekap.form_koordinator',[
            'kecamatan'=>$kecamatan,
            'kelurahan'=>$kelurahan,
            'kabupaten'=>$kabupaten
        ]);
    }

    public function masyarakat(Request $request)
    {
        $kecamatan=DB::table('wilayah_kelurahan')->select('kode_kec', 'nama_kec')
        ->distinct()
        ->orderby('kode_kec')
        ->get();

        $kabupaten=DB::table('wilayah_kelurahan')->select('kode_kab', 'nama_kab')
        ->distinct()
        ->orderby('kode_kab')
        ->get();

        $kelurahan=DB::table('wilayah_kelurahan')->select('kode_kel', 'nama_kel')
        ->orderby('kode_kec')
        ->orderby('kode_kel')
        ->get();
        return view('dashboard.rekap.form_masyarakat',[
            'kecamatan'=>$kecamatan,
            'kelurahan'=>$kelurahan,
            'kabupaten'=>$kabupaten
        ]);
    }

    public function lihatHarian(Request $request)
    {
        $dataPilih=DB::query()
            ->fromSub(
                DB::table('transaksi_penerimaans')
                    ->select(DB::raw(
                        'DATE_FORMAT(created_at, "%Y-%m-%d %H:%i") as waktu,kode_penerimaan, no_transaksi_masuk,deskripsi_penerimaan,jumlah_penerimaan, 0 as jumlah_pengeluaran'
                        ))
            ->union(
                DB::table('transaksi_pengeluarans')
                    ->select(DB::raw(
                        'DATE_FORMAT(created_at, "%Y-%m-%d %H:%i") as waktu,kode_pengeluaran, no_transaksi_keluar,deskripsi_pengeluaran, 0 as jumlah_penerimaan,jumlah_pengeluaran'
                        ))
            ),
        'inner'
    )
    ->select(['inner.*'])
    ->get();
    dd($dataPilih);

    }

    public function penerimaan(Request $request)
    {
        return view('dashboard.jurnal.form_jurnal_penerimaan');

    }

    public function pengeluaran(Request $request)
    {
        return view('dashboard.jurnal.form_jurnal_pengeluaran');

    }

    public function rekap(Request $request)
    {
        return view('dashboard.jurnal.form_jurnal_rekap');
    }

    public function pdfharian(Request $request)
	{


		/* $headers=[
            'HEADER_LOGO'=>\App\Helpers\Helper::public_path("images/header_pmb.png")
			];

		$pdf = \Meneses\LaravelMpdf\Facades\LaravelMpdf::loadView('dashboard.report.report_jurnal_harian',[
								'headers'=>$headers,
								'tanggal'=>\App\Helpers\Helper::tanggal('d F Y')
							],
							[],
							[
							    'title' => 'Jurnal Harian',
							]); */
				$file_pdf=\App\Helpers\Helper::public_path("exported/pdf/jurnalharian_.pdf");
				//$pdf->save($file_pdf);

				$pdf_file="storage/exported/pdf/jurnalharian_.pdf";

				return Response()->json([
										'status'=>1,
										'pid'=>'fetchdata',
										'pdf_file'=>$pdf_file
									], 200);
	}



    public function pdf_harian(Request $request)
    {
        $tanggal=explode('-',$request->daterange);

        $tanggal_satu=Carbon::parse($tanggal[0])->format('Y-m-d');
        $tanggal_dua=Carbon::parse($tanggal[1])->format('Y-m-d');
        $id='03';
        $unit=MasterUnit::select(DB::raw('nama_unit,alamat_unit,jabatan_tanda_wadek,nama_tanda_wadek,jabatan_tanda_kepala,nama_tanda_kepala'))->where('kode_unit',$id)->first();


        $dataPilih=DB::query()
        ->fromSub(
            DB::table('transaksi_penerimaans')
                ->select(DB::raw(
                    'DATE_FORMAT(created_at, "%Y-%m-%d %H:%i") as waktu,kode_penerimaan, no_transaksi_masuk,deskripsi_penerimaan,jumlah_penerimaan, 0 as jumlah_pengeluaran'
                    ))
        ->union(
            DB::table('transaksi_pengeluarans')
                ->select(DB::raw(
                    'DATE_FORMAT(created_at, "%Y-%m-%d %H:%i") as waktu,kode_pengeluaran, no_transaksi_keluar,deskripsi_pengeluaran, 0 as jumlah_penerimaan,jumlah_pengeluaran'
                    ))
        ),
    'inner'
)
->select(['inner.*'])
->whereBetween('inner.waktu', [$tanggal_satu, $tanggal_dua])
->orderby('inner.waktu','asc')
->get();
        $pdf = Pdf::loadView('dashboard.report.report_jurnal_harian',[
            'datapilih'=>$dataPilih,
            'tanggal_1'=>\App\Helpers\Helper::tanggal('d F Y',$tanggal_satu),
            'tanggal_2'=>\App\Helpers\Helper::tanggal('d F Y',$tanggal_dua),
            'unit_fakultas'=>$unit,
            'tanggal'=>\App\Helpers\Helper::tanggal('d F Y')
        ]);
        return $pdf->stream('file_name.pdf', array('Attachment' => 0));
    }

    public function pdf_pengeluaran(Request $request)
    {
        $tanggal=explode('-',$request->daterange);

        $tanggal_satu=Carbon::parse($tanggal[0])->format('Y-m-d');
        $tanggal_dua=Carbon::parse($tanggal[1])->format('Y-m-d');
        $id='03';
        $unit=MasterUnit::select(DB::raw('nama_unit,alamat_unit,jabatan_tanda_wadek,nama_tanda_wadek,jabatan_tanda_kepala,nama_tanda_kepala'))->where('kode_unit',$id)->first();


        $dataPilih=DB::table('transaksi_pengeluarans')
                ->select(DB::raw('DATE_FORMAT(created_at, "%Y-%m-%d %H:%i") as waktu,kode_pengeluaran, no_transaksi_keluar,deskripsi_pengeluaran, 0 as jumlah_penerimaan,jumlah_pengeluaran'))
                ->whereBetween('created_at', [$tanggal_satu, $tanggal_dua])
                ->orderby('created_at','asc')
                ->get();

        $pdf = Pdf::loadView('dashboard.report.report_jurnal_pengeluaran',[
            'datapilih'=>$dataPilih,
            'tanggal_1'=>\App\Helpers\Helper::tanggal('d F Y',$tanggal_satu),
            'tanggal_2'=>\App\Helpers\Helper::tanggal('d F Y',$tanggal_dua),
            'unit_fakultas'=>$unit,
            'tanggal'=>\App\Helpers\Helper::tanggal('d F Y')
        ]);
        return $pdf->stream('file_name.pdf', array('Attachment' => 0));
    }

    public function pdf_penerimaan(Request $request)
    {
        $tanggal=explode('-',$request->daterange);

        $tanggal_satu=Carbon::parse($tanggal[0])->format('Y-m-d');
        $tanggal_dua=Carbon::parse($tanggal[1])->format('Y-m-d');
        $id='03';
        $unit=MasterUnit::select(DB::raw('nama_unit,alamat_unit,jabatan_tanda_wadek,nama_tanda_wadek,jabatan_tanda_kepala,nama_tanda_kepala'))->where('kode_unit',$id)->first();


        $dataPilih=DB::table('transaksi_penerimaans')
                ->select(DB::raw('DATE_FORMAT(created_at, "%Y-%m-%d %H:%i") as waktu,kode_penerimaan, no_transaksi_masuk,deskripsi_penerimaan,jumlah_penerimaan, 0 as jumlah_pengeluaran'))
                ->whereBetween('created_at', [$tanggal_satu, $tanggal_dua])
                ->orderby('created_at','asc')
                ->get();

        $pdf = Pdf::loadView('dashboard.report.report_jurnal_penerimaan',[
            'datapilih'=>$dataPilih,
            'tanggal_1'=>\App\Helpers\Helper::tanggal('d F Y',$tanggal_satu),
            'tanggal_2'=>\App\Helpers\Helper::tanggal('d F Y',$tanggal_dua),
            'unit_fakultas'=>$unit,
            'tanggal'=>\App\Helpers\Helper::tanggal('d F Y')
        ]);
        return $pdf->stream('file_name.pdf', array('Attachment' => 0));
    }

    public function pdf_rekap(Request $request)
    {
        $tanggal=explode('-',$request->daterange);

        $tanggal_satu=Carbon::parse($tanggal[0])->format('Y-m-d');
        $tanggal_dua=Carbon::parse($tanggal[1])->format('Y-m-d');
        $id='03';
        $unit=MasterUnit::select(DB::raw('nama_unit,alamat_unit,jabatan_tanda_wadek,nama_tanda_wadek,jabatan_tanda_kepala,nama_tanda_kepala'))->where('kode_unit',$id)->first();


        $dataPilih_penerimaan=DB::table('master_penerimaans')
                ->select(DB::raw('kode_penerimaan as kode_tb,nama_pemasukan as jenis_transaksi, sum(jumlah_penerimaan) as jumlah_penerimaan'))
                ->rightjoin('transaksi_penerimaans','kode_penerimaan','=','master_penerimaans.kode')
                ->whereBetween('transaksi_penerimaans.created_at', [$tanggal_satu, $tanggal_dua])
                ->groupby('kode_penerimaan','nama_pemasukan')
                ->orderby('kode_penerimaan','asc')
                ->get();
        $dataPilih_pengeluaran=DB::table('master_pengeluarans')
                ->select(DB::raw('kode_pengeluaran as kode_tb,nama_pengeluaran as jenis_transaksi, sum(jumlah_pengeluaran) as jumlah_penerimaan'))
                ->rightjoin('transaksi_pengeluarans','kode_pengeluaran','=','master_pengeluarans.kode')
                ->whereBetween('transaksi_pengeluarans.created_at', [$tanggal_satu, $tanggal_dua])
                ->groupby('kode_pengeluaran','nama_pengeluaran')
                ->orderby('kode_pengeluaran','asc')
                ->get();

        $pdf = Pdf::loadView('dashboard.report.report_jurnal_rekap',[
            'datapilih_penerimaan'=>$dataPilih_penerimaan,
            'datapilih_pengeluaran'=>$dataPilih_pengeluaran,
            'tanggal_1'=>\App\Helpers\Helper::tanggal('d F Y',$tanggal_satu),
            'tanggal_2'=>\App\Helpers\Helper::tanggal('d F Y',$tanggal_dua),
            'unit_fakultas'=>$unit,
            'tanggal'=>\App\Helpers\Helper::tanggal('d F Y')
        ]);
        return $pdf->stream('file_name.pdf', array('Attachment' => 0));

    }
}
