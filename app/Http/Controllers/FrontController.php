<?php

namespace App\Http\Controllers;

use App\Helpers\Helper;
use App\Models\Masyarakat;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Mews\Captcha\Facades\Captcha;
use Ramsey\Uuid\Uuid;

class FrontController extends Controller
{
    public function index()
    {
        return view('auth/frontpage');
    }

    public function login()
    {
        return view('auth/login-koordinator');
    }

    public function cekKoordinator(Request $request)
    {
        $request->validate([
            'nohp' => 'required'
        ]);
        $cekPIN=Masyarakat::select(DB::raw('*'))
        ->where('nomor_wa',$request->nohp)
        ->where('koordinator','=','1');

        try {
            if($cekPIN->count()=='1')
            {
                $getData=$cekPIN->first();
                $request->session()->put([
                    'id_masyarakat' => $getData->id_masyarakat,
                    'nik' => $getData->nik,
                    'nama_lengkap' => $getData->nama_lengkap,
                    'nomor_wa' => $getData->nomor_wa,
                    'koordinator' => $getData->koordinator,
                    'kabupaten' => $getData->kabupaten,
                    'kecamatan' => $getData->kecamatan,
                    'kelurahan' => $getData->kelurahan,
                    'rt' => $getData->rt,
                    'rw' => $getData->rw,
                ]);

                return redirect('/dashboard-koordinator')->with('success', 'Login Berhasil Masuk ..');

            }else
            {
                return redirect('/login-koordinator')->with('error', 'Nomor Tidak Dikenali');
            }

        } catch (\Throwable $th) {
            return redirect('/login-koordinator')->with('error', 'Terdapat Kesalahan');
        }



    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'email' => 'required|email',
            'password' => 'required|same:password_confirmation',
            'password_confirmation' => 'required',
            'g-recaptcha-response' => 'required|recaptchav3:register,0.5'
        ]);

        dd('done');
    }

    public function storeAspirasi(Request $request)
    {
        $request->validate([
            'email' => '',
            'nomor_handphone' => '',
            'aspirasi' => 'required',
            'captcha' => 'required|captcha'
        ]);

        try 
        {
            $id=Uuid::uuid4()->toString();
            $now = \Carbon\Carbon::now()->toDateTimeString(); 
                DB::table('aspirasis')->insert([
                    'id_aspirasi' => $id,
                    'email' => $request->email,
                    'nomor_handphone' => $request->nomor_handphone,
                    'aspirasi' => $request->aspirasi,
                    'created_at'=>$now
                ]);  
            //$message="Bismillahirrahmanirrahim, Saya H. Yuyun Hidayat, Drs. Ketua Pengurus Yayasan Pendidikan Sebelas April (YPSA) Sumedang yang menaungi UNSAP,STAI Sebelas April, SMK, dan SMP, dicalonkan oleh PPP menjadi anggota DPR RI periode 2024-2029 DAPIL JABAR IX (Sumedang-Majalengka-Subang). Mohon doa dan dukungannya dengan klik link (https://yuyunhidayat.id) untuk mendaftar menjadi Relawan. Hatur Nuhun.";
            //Helper::kirimWA('6285524881544',$message);

            return response()->json(['status'=>'200','success'=>'Data Berhasil Masuk']);

        } catch (Exception $e) {
            return response()->json(['status'=>'201','error'=>$e->getMessage()]);
            
        }

    }

    public function reloadCaptcha()
    {
        return response()->json(['captcha'=> captcha_img()]);
    }

}
