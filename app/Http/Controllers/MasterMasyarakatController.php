<?php

namespace App\Http\Controllers;

use App\Models\MasterPengeluaran;
use App\Models\Masyarakat;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Ramsey\Uuid\Uuid;
use Yajra\DataTables\DataTables;

class MasterMasyarakatController extends Controller
{
    public function index(Request $request)
    {

        if ($request->ajax()) {
            $data = Masyarakat::select(DB::raw('id_masyarakat,nik,nama_lengkap,nomor_wa,concat(nama_kab,", ",nama_kec,", ",nama_kel) as alamat,koordinator'))
            ->join('wilayah_kelurahan','wilayah_kelurahan.kode_kel','masyarakats.kelurahan')
            ->where('koordinator','<>','1')
            ->orderby('created_at','desc')
            ->get();;
            return DataTables::of($data)
                    ->addIndexColumn()
                    ->addColumn('action', function($row){

                           $btn = '<a href="javascript:void(0)" data-toggle="tooltip"  data-id="'.$row->id_masyarakat.'" data-original-title="Edit" class="edit btn btn-primary btn-sm editCustomer"><i class="fas fa-edit"></i>Edit</a>';

                           $btn = $btn.' <a href="javascript:void(0)" data-toggle="modal" data-target="#modal-delete" data-id="'.$row->id_masyarakat.'" data-original-title="Delete" class="btn btn-danger btn-sm deleteCustomer"><i class="fa fa-trash" aria-hidden="true"></i> Delete</a>';

                            return $btn;
                    })
                    ->rawColumns(['action'])
                    ->make(true);
        }
        return view('dashboard.masyarakat.index');
    }

    public function store(Request $request)
    {
        try {
            if ($request->idPengeluaran=='')
        {
            MasterPengeluaran::create([
                'id_pengeluaran' => Uuid::uuid4()->toString(),
                'kode' => $request->kode,'nama_pengeluaran' => $request->nama_pengeluaran,'penjelasan' => $request->penjelasan
                ]);
        }else
        {
            MasterPengeluaran::updateOrCreate(
                ['id_pengeluaran' => $request->idPengeluaran],
                ['kode' => $request->kode,'nama_pengeluaran' => $request->nama_pengeluaran,'penjelasan' => $request->penjelasan]
            );
        }
        return response()->json(['status'=>'200','success'=>'Data Sukses di Simpan']);
        } catch (\Throwable $th) {
            return response()->json(['status'=>'404','success'=>'Terdapat Kesalahan']);
        }

    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Customer  $Customer
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $pengeluaran = MasterPengeluaran::find($id);
        return response()->json($pengeluaran);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Customer  $Customer
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        MasterPengeluaran::find($id)->delete();

        return response()->json(['success'=>'Data Pengeluaran Sukses Dihapus !']);
    }
}
