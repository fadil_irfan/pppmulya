<?php

namespace App\Http\Controllers;

use Exception;
use Ramsey\Uuid\Uuid;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class MasyarakatController extends Controller
{
    public function register()
    {
        $kecamatan=DB::table('wilayah_kelurahan')->select('kode_kec', 'nama_kec')
        ->distinct()
        ->orderby('kode_kec')
        ->get();

        $kabupaten=DB::table('wilayah_kelurahan')->select('kode_kab', 'nama_kab')
        ->distinct()
        ->orderby('kode_kab')
        ->get();

        $kelurahan=DB::table('wilayah_kelurahan')->select('kode_kel', 'nama_kel')
        ->orderby('kode_kec')
        ->orderby('kode_kel')
        ->get();

        return view('auth/register',[
            'kecamatan'=>$kecamatan,
            'kelurahan'=>$kelurahan,
            'kabupaten'=>$kabupaten
        ]);
    }

    public function wilayahKabupaten(Request $request)
    {
        $kode_kab =$request->kabupaten ;
        if ($kode_kab=='')
        {
            $provinsi='Pilih Kecamatan';
        }else
        {
            $provinsi=DB::table('wilayah_kelurahan')
            ->select('kode_kab','kode_kec', 'nama_kec')
            ->where('kode_kab','like',$kode_kab.'%')
            ->orderBy('nama_kec')
            ->distinct()
            ->get();
        }

        return Response()->json([
            'error_code'=>0,
            'error_desc'=>'',
            'data'=>$provinsi,
            'message'=>'fetch data berhasil'
        ], 200);

    }

    public function wilayahKelurahan(Request $request)
    {
        $kode_kec =$request->kecamatan ;
        if ($kode_kec=='')
        {
            $provinsi='Pilih Desa/Kelurahan';
        }else
        {
            $provinsi=DB::table('wilayah_kelurahan')
            ->where('kode_kec','like',$kode_kec.'%')
            ->orderBy('nama_kel')
            ->get();
        }

       


        return Response()->json([
            'error_code'=>0,
            'error_desc'=>'',
            'data'=>$provinsi,
            'message'=>'fetch data berhasil'
        ], 200);
        

        /* $getWilayahKota = new NeoFeeder([
            'act' => 'GetWilayah',
            'filter' => "id_wilayah like '$idKota%' and nama_wilayah like 'Kec.%'",
            'order' => "nama_wilayah"
        ]); */

    }

    public function storeMasyarakat(Request $request)
    {
        $request->validate([
            'nik' => 'required|min:16|max:16|unique:masyarakats',
            'nama_lengkap' => 'required',
            'nomor_wa' => 'required|starts_with:0|min:10|unique:masyarakats',
            'kabupaten' => 'required',
            'kecamatan' => 'required',
            'kelurahan' => 'required',
            'rt' => 'required',
            'rw' => 'required',
        ]);

        try 
        {
            $id=Uuid::uuid4()->toString();
            $now = \Carbon\Carbon::now()->toDateTimeString(); 
                DB::table('masyarakats')->insert([
                    'id_masyarakat' => $id,
                    'nik' => $request->nik,
                    'nama_lengkap' => $request->nama_lengkap,
                    'nomor_wa' => $request->nomor_wa,
                    'kecamatan' => $request->kecamatan,
                    'kabupaten' => $request->kabupaten,
                    'kelurahan' => $request->kelurahan,
                    'rt' => $request->rt,
                    'rw' => $request->rw,
                    'created_at'=>$now,
                    'koordinator' => '1',
                ]);  
                
            return response()->json(['status'=>'200','success'=>'Data Berhasil Masuk']);

        } catch (Exception $e) {
            return response()->json(['status'=>'201','error'=>$e->getMessage()]);
            
        }

    }
}
