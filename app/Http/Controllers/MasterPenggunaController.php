<?php

namespace App\Http\Controllers;

use Exception;
use App\Models\User;
use Ramsey\Uuid\Uuid;
use App\Models\MasterUnit;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class MasterPenggunaController extends Controller
{
    public function index(Request $request)
    {


        if ($request->ajax()) {
            $data = User::select(DB::raw('id,name,email,role'))
            ->get();
            return DataTables::of($data)
                    ->addIndexColumn()
                    ->addColumn('action', function($row){

                           $btn = '<a href="javascript:void(0)" data-toggle="tooltip"  data-id="'.$row->id.'" data-original-title="Edit" class="edit btn btn-primary btn-sm editCustomer"><i class="fas fa-edit"></i>Edit</a>';

                           $btn = $btn.' <a href="javascript:void(0)" data-toggle="modal" data-target="#modal-delete" data-id="'.$row->id.'" data-original-title="Delete" class="btn btn-danger btn-sm deleteCustomer"><i class="fa fa-trash" aria-hidden="true"></i> Delete</a>';

                            return $btn;
                    })
                    ->rawColumns(['action'])
                    ->make(true);
        }
        
        return view('dashboard.masterpengguna.index',[
        ]);
    }

    public function store(Request $request)
    {
        try {
            if ($request->id=='')
            {
                $id=Uuid::uuid4()->toString();
            }else
            {
                $id=$request->id;
            }

            User::updateOrCreate(
                ['id' => $id,'name' => $request->name],
                ['email' => $request->email,'role' => $request->role,'id_unit' => $request->id_unit,'password' => Hash::make($request->password)]
            );
            return response()->json(['status'=>'200','success'=>'Data Sukses di Simpan']);
        } catch (Exception $e) {
            return response()->json(['status'=>'404','success'=>$e->getMessage()]);
        }


    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Customer  $Customer
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::find($id);
        return response()->json($user);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Customer  $Customer
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        User::find($id)->delete();
        return response()->json(['success'=>'Data User Sukses Dihapus !']);
    }
}
