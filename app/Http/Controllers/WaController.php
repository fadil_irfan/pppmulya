<?php

namespace App\Http\Controllers;

use App\Models\Wa;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class WaController extends Controller
{
    public function getKontak (Request $request)
    {
        $ambil_user=Wa::select(DB::raw('nomor,
                                            nama_lengkap, 
                                            sumber_data, 
                                            status, 
                                            response
                                    '))
                                    ->get();

        return Response()->json([
            'status'=>0,
            'pid'=>'fetchdata', 
            'data'=>$ambil_user,
            'message'=>'Fetch data whatsapp'
        ], 200); 
    }

    public function sendWa(Request $request)
    {
        $pilih_kontak=Wa::select(DB::raw('nomor,nama_lengkap,sumber_data,status,response'))
                                ->whereNull('status')
                                ->inRandomOrder()
								->first();

                                //dd($pilih_kontak);
                                $token = "BqeUwjjvD6ojAES49XdenbrFf1HSycFAZa5pi7ExGdFV2Uafjs";
                                          
        $message="Bismillahirrahmanirrahim, Saya H. Yuyun Hidayat, Drs. Ketua Pengurus Yayasan Pendidikan Sebelas April (YPSA) Sumedang yang menaungi UNSAP,STAI Sebelas April, SMK, dan SMP, dicalonkan oleh PPP menjadi anggota DPR RI periode *2024-2029* DAPIL JABAR IX (Sumedang-Majalengka-Subang). Mohon doa dan dukungannya dengan klik Tombol Kunjungi Website (https://yuyunhidayat.id) untuk mendaftar menjadi Relawan. Hatur Nuhun.";
        //$phone="6285220717928";
        $file="https://yuyunhidayat.id/logo-wa.jpeg";
        $curl = curl_init();
        curl_setopt_array($curl, array(
          CURLOPT_URL => 'https://app.ruangwa.id/api/send_image',
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => '',
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 0,
          CURLOPT_FOLLOWLOCATION => true,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => 'POST',
          CURLOPT_POSTFIELDS => 'token='.$token.'&number='.$pilih_kontak->nomor.'&file='.$file.'&caption='.$message,
        ));
        $response = curl_exec($curl);
        curl_close($curl);            

                                $jawab=json_decode($response, true);
                                if ($jawab['result']=='false')
                                {
                                    $pilih =Wa::find($pilih_kontak->nomor);
                                    $pilih->response=$response;
                                    $pilih->status='2';
                                    $pilih->update();
                                }else
                                {
                                    $pilih =Wa::find($pilih_kontak->nomor);
                                    $pilih->response=$response;
                                    $pilih->status='1';
                                    $pilih->update();
                                }
                                
                                

                                return Response()->json([
                                    'status'=>0,
                                    'pid'=>'kirimpesan', 
                                    'data'=>$pilih,
                                    'message'=>'Kirim pesan whatsapp '
                                ], 200);
                            
        
    }

}
