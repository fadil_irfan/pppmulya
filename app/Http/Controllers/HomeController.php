<?php

namespace App\Http\Controllers;

use App\Models\Aspirasi;
use App\Models\MasterUnit;
use App\Models\Masyarakat;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use Yajra\DataTables\DataTables;

class HomeController extends Controller
{
    
    public function index()
    {
        
        $unit="";
        $relawan=Masyarakat::where('koordinator','=','1')
        ->count();
        $masyarakat=Masyarakat::where('koordinator','<>','1')
        ->count();
        $total=intval($relawan)+intval($masyarakat);
        $wilayah=Masyarakat::select(DB::raw('kabupaten,kecamatan,kelurahan'))->get();
        return view('dashboard.home.index',[
            'unit'=>$unit,
            'relawan'=>$relawan,
            'masyarakat'=>$masyarakat,
            'total'=>$total,
            'wilayah'=>$wilayah
        ]);
    }

    public function aspirasi(Request $request)
    {

        if ($request->ajax()) {
            $data = Aspirasi::select(DB::raw('id_aspirasi,email,nomor_handphone,aspirasi,created_at'))
            ->orderby('created_at','desc')
            ->get();
            return DataTables::of($data)
                    ->addIndexColumn()
                    ->addColumn('action', function($row){

                           $btn = '';

                           $btn = $btn.' <a href="javascript:void(0)" data-toggle="modal" data-target="#modal-delete" data-id="'.$row->id_aspirasi.'" data-original-title="Delete" class="btn btn-danger btn-sm deleteCustomer"><i class="fa fa-trash" aria-hidden="true"></i> Delete</a>';

                            return $btn;
                    })
                    ->editColumn('created_at', function($data){ $formatedDate = Carbon::createFromFormat('Y-m-d H:i:s', $data->created_at)->format('d-m-Y H:i:s'); return $formatedDate; })
                    ->rawColumns(['action'])
                    ->make(true);
        }
        return view('dashboard.aspirasi.index');
    }

    public function keluar()
    {
        Auth::logout();
        Session::flush();
        return redirect('/login')->with('success', 'Anda Berhasil Logout ..');

    }
}
