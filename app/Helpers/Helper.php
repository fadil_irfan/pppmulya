<?php

namespace App\Helpers;

use App\Models\Masyarakat;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use URL;

class Helper {
    /**
     * daftar bulan
     */
    private static $daftar_bulan=[
        1=>'Januari',
        2=>'Februari',
        3=>'Maret',
        4=>'April',
        5=>'Mei',
        6=>'Juni',
        7=>'Juli',
        8=>'Agustus',
        9=>'September',
        10=>'Oktober',
        11=>'November',
        12=>'Desember'
    ];
    /**
     * daftar bulan
     */
    private static $daftar_bulan_spp=[        
        9=>'September',
        10=>'Oktober',
        11=>'November',
        12=>'Desember',
        1=>'Januari',
        2=>'Februari',
        3=>'Maret',
        4=>'April',
        5=>'Mei',
        6=>'Juni',
        7=>'Juli',
        8=>'Agustus',
    ];
    /*
     * nama hari dalam bahasa indonesia
     */
    private static $namaHari = [
        1=>'Senin',
        2=>'Selasa',
        3=>'Rabu',
        4=>'Kamis',
        5=>'Jumat',
        6=>'Sabtu',
        7=>'Minggu',
    ];
    public static function getNamaBulan($no_bulan=null)
    {
        if ($no_bulan===null)
        {
            return Helper::$daftar_bulan;
        }
        else if ($no_bulan >=1 && $no_bulan <=12)
        {
            return Helper::$daftar_bulan[$no_bulan];
        }
        else
        {
            return null;
        }
    }
    public static function getNamaBulanSPP($no_bulan=null)
    {
        if ($no_bulan===null)
        {
            return Helper::$daftar_bulan_spp;
        }
        else if ($no_bulan >=1 && $no_bulan <=12)
        {
            return Helper::$daftar_bulan_spp[$no_bulan];
        }
        else
        {
            return null;
        }
    }
    /**
	* digunakan untuk mendapatkan nama hari
	*/
	public static function getNamaHari ($no_hari=null) {
		if ($no_hari===null)
        {
			return Helper::$namaHari;
        }
        else if ($no_hari >=1 && $no_hari <=7)
        {
			return Helper::$namaHari[$no_hari];
        }
        else
        {
            return null;
        }
    }
    /**
     * get nomor bulan dari tanggal
     */
    public static function getNomorBulan($date)
    {
        Carbon::setLocale(app()->getLocale());
        $tanggal = Carbon::parse($date);
        return $tanggal->format('n');
    }
    /**
     * digunakan untuk mendapatkan format tahun akademik
     */
    
    /**
     * digunakan untuk memformat tanggal
     * @param type $format
     * @param type $date
     * @return type date
     */
    public static function tanggal($format, $date=null) {
        Carbon::setLocale(app()->getLocale());
        if ($date == null){
            $tanggal=Carbon::parse(Carbon::now())->format($format);
        }else{
            $tanggal = Carbon::parse($date)->format($format);
        }
        $result = str_replace([
                                'Sunday',
                                'Monday',
                                'Tuesday',
                                'Wednesday',
                                'Thursday',
                                'Friday',
                                'Saturday'
                            ],
                            [
                                'Minggu',
                                'Senin',
                                'Selasa',
                                'Rabu',
                                'Kamis',
                                'Jumat',
                                'Sabtu'
                            ],
                            $tanggal);

        return str_replace([
                            'January',
                            'February',
                            'March',
                            'April',
                            'May',
                            'June',
                            'July',
                            'August',
                            'September',
                            'October',
                            'November' ,
                            'December'
                        ],
                        [
                            'Januari',
                            'Februari',
                            'Maret',
                            'April',
                            'Mei',
                            'Juni',
                            'Juli',
                            'Agustus',
                            'September',
                            'Oktober',
                            'November',
                            'Desember'
                        ], $result);
    }   
    /**
     * digunakan untuk mengecek format tanggal valid
     */
    public static function checkformattanggal ($tanggal) {
        
        $data = explode('-',$tanggal);            
        return checkdate($data[1],$data[2],$data[0]);
    }
    /**
	* digunakan untuk mem-format uang
	*/
	public static function formatUang ($uang=0,$decimal=2) {
		$formatted = number_format((float)$uang,$decimal,',','.');
        return $formatted;
    }
    /**
	* digunakan untuk mem-format angka
	*/
	public static function formatAngka ($angka=0) {
        $bil = intval($angka);
        $formatted = ($bil < $angka) ? $angka : $bil;
        return $formatted;
    }
    /**
	* digunakan untuk mem-format persentase
	*/
	public static function formatPersen ($pembilang,$penyebut=0,$dec_sep=2) {
        $result=0.00;
		if ($pembilang > 0 && $penyebut > 0) {
            $temp=round(number_format((float)($pembilang/$penyebut)*100,2),$dec_sep);
            $result = $temp;
        }
        else
        {
            $result=0;
        }
        return $result;
	}
    /**
	* digunakan untuk mem-format pecahan
	*/
	public static function formatPecahan ($pembilang,$penyebut=0,$dec_sep=2) {
        $result=0;
		if ($pembilang > 0 && $penyebut > 0) {
            $result=round(number_format((float)($pembilang/$penyebut),2),$dec_sep);
        }
        return $result;
    }

    public static function public_path($path = null)
    {
        return rtrim(app()->basePath('storage/app/public/' . $path), '/');
    }
    public static function exported_path($folder='/')
    {
        return app()->basePath("storage/app/public/exported$folder");
    }

    public static function getKabupaten($kode)
    {
        $kabupaten=DB::table('wilayah_kelurahan')
        ->where('kode_kab','=',$kode)
        ->first();
        if (is_null($kabupaten))
        {
            return 'Kabupaten tidak ditemukan';
        }else
        {
            return Str::upper($kabupaten->nama_kab);
        }
        
    }

    public static function getKecamatan($kode)
    {
        $kecamatan=DB::table('wilayah_kelurahan')
        ->where('kode_kec','=',$kode)
        ->first();
        if (is_null($kecamatan))
        {
            return 'Kecamatan tidak ditemukan';
        }else
        {
            return Str::upper($kecamatan->nama_kec);
        }
        
    }

    public static function getKelurahan($kode)
    {
        $kelurahan=DB::table('wilayah_kelurahan')
        ->where('kode_kel','=',$kode)
        ->first();
        if (is_null($kelurahan))
        {
            return 'Kelurahan tidak ditemukan';
        }else
        {
            return Str::upper($kelurahan->nama_kel);
        }
        
    }

    public static function getHitungWilayah($kab,$kec,$kel)
    {
        $kelurahan=Masyarakat::where('kabupaten','=',$kab)
        ->where('kecamatan','=',$kec)
        ->where('kelurahan','=',$kel)
        ->count();
            return Str::upper($kelurahan);
        
    }

    public static function kirimWA($phone,$message)
    {
        $api_key = "OU6TII4TCY2IGPMD";
        $number_key="Cj2IhqzJGdxcpHgM";
                                
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => 'https://api.watzap.id/v1/send_image_url',
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS =>'{
                "api_key": "'.$api_key.'",
                "number_key": "'.$number_key.'",
                "phone_no": "'.$phone.'",
                "url": "https://yuyunhidayat.id/logo-wa.jpeg",
                "message": "'.$message.'",
                "separate_caption": "0"
            }',
              CURLOPT_HTTPHEADER => array(
                'Content-Type: application/json',
              ),
            ));
            $response = curl_exec($curl);
            curl_close($curl);
            return $response;
            
    }

    public static function kirimWAButton($phone)
    {
        $token = "mrBwWG5RDMeCtnK88fkJPY6AEodcYwdTttjgJju1FV5G9jNhLQ";
        $text= "Testing kirim button";
        $buttonlabel= "Google,Facebook";
        $buttonurl= "https://www.google.com,https://www.facebook.com";

        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => 'https://app.ruangwa.id/api/send_buttonurl',
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS => 'token='.$token.'&number='.$phone.'&text='.$text.'&buttonlabel='.$buttonlabel.'&buttonurl='.$buttonurl,
            CURLOPT_HTTPHEADER => array(
                    'Content-Type: application/x-www-form-urlencoded'
                ),
        ));
            $response = curl_exec($curl);
            curl_close($curl);
                                
        
            return $response;
            
    }




}
