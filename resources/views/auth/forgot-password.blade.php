@extends('layouts.app', ['title' => 'Forgot Password'])

@section('content')
<div class="col col-sm-6 col-md-6 col-lg-6 col-xl-6">
    <div class="card">

        <div class="card-body">
            @if (session('status'))
            <div class="alert alert-success" role="alert">
                {{ session('status') }}
            </div>
            @endif

            <form method="POST" action="{{ route('password.email') }}">
                @csrf

                <div class="form-group">
                    <label class="font-weight-bold text-uppercase">Alamat Email</label>
                    <input id="email" type="email" class="form-control @error('email') is-invalid @enderror"
                        name="email" value="{{ old('email') }}" required autocomplete="email" autofocus
                        placeholder="Masukkan Alamat Email">

                    @error('email')
                    <div class="alert alert-danger mt-2">
                        <strong>{{ $message }}</strong>
                    </div>
                    @enderror
                </div>

                <button type="submit" class="btn btn-primary btn-block"> <i class="fas fa-paper-plane"></i> Kirim Password Reset</button>
                <a href="{{ route('login')}}" class="btn btn-info btn-block"> <i class="fas fa-sign-in-alt"></i> Login</a>
            </form>
        </div>
    </div>
</div>
@endsection
