<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport"
        content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
        <meta name="google-site-verification" content="7l2C1jGVsCEJcQg__q8pYx06XRKYwpstLsplBi0H1cs" />
    <title>H. Yuyun Hidayat, Drs. - Ketua YPSA (UNSAP)</title>
    <!-- Favicon -->
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <link rel="apple-touch-icon" sizes="57x57" href="{{ url('frontpage/favicon/apple-icon-57x57.png') }}">
    <link rel="apple-touch-icon" sizes="60x60" href="{{ url('frontpage/favicon/apple-icon-60x60.png.png') }}">
    <link rel="apple-touch-icon" sizes="72x72" href="{{ url('frontpage/favicon/apple-icon-72x72.png.png') }}">
    <link rel="apple-touch-icon" sizes="76x76" href="{{ url('frontpage/favicon/apple-icon-76x76.png') }}">
    <link rel="apple-touch-icon" sizes="114x114" href="{{ url('frontpage/favicon/apple-icon-114x114.png') }}">
    <link rel="apple-touch-icon" sizes="120x120" href="{{ url('frontpage/favicon/apple-icon-120x120.png') }}">
    <link rel="apple-touch-icon" sizes="144x144" href="{{ url('frontpage/favicon/apple-icon-144x144.png') }}">
    <link rel="apple-touch-icon" sizes="152x152" href="{{ url('frontpage/favicon/apple-icon-152x152.png') }}">
    <link rel="apple-touch-icon" sizes="180x180" href="{{ url('frontpage/favicon/apple-icon-180x180.png') }}">
    <link rel="icon" type="image/png" sizes="192x192"
        href="{{ url('frontpage/favicon/android-icon-192x192.png') }}">
    <link rel="icon" type="image/png" sizes="32x32"
        href="{{ url('frontpage/favicon/favicon-32x32.png"') }}>
<link rel="icon" type="image/png" sizes="96x96"
        href="{{ url('frontpage/favicon/favicon-96x96.png') }}">
    <link rel="icon" type="image/png" sizes="16x16" href="{{ url('frontpage/favicon/favicon-16x16.png') }}">
    <link rel="manifest" href="{{ url('frontpage/favicon/manifest.json') }}">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="{{ url('frontpage/favicon/ms-icon-144x144.png') }}">
    <meta name="theme-color" content="#ffffff">

    <meta property="og:image" content="{{ url('logo-wa.jpeg') }}">
    <!-- Fonts (google font) -->
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link
        href="https://fonts.googleapis.com/css2?family=Jost:wght@200;300;400;500;600;700;800;900&family=Urbanist:wght@400;500;600;700&display=swap"
        rel="stylesheet">
    <!-- Bootstrap Stylesheet -->
    <link rel="stylesheet" href="{{ url('frontpage/css/bootstrap.min.css') }}">
    <!-- Magnific-popup stylesheet -->
    <link rel="stylesheet" href="{{ url('frontpage/css/magnific-popup.css') }}">
    <!-- Owl Carousel stylesheet -->
    <link rel="stylesheet" href="{{ url('frontpage/css/owl.carousel.css') }}">
    <!-- Owl theme stylesheet -->
    <link rel="stylesheet" href="{{ url('frontpage/css/owl.theme.default.min.css') }}">
    <!-- Animated headline stylesheet -->
    <link rel="stylesheet" href="{{ url('frontpage/css/jquery.animatedheadline.css') }}">
    <!-- MeanMenu stylesheet -->
    <link rel="stylesheet" href="{{ url('frontpage/css/meanmenu.min.css') }}">
    <!-- Line progressbar stylesheet -->
    <link rel="stylesheet" href="{{ url('frontpage/css/jquery.lineProgressbar.css') }}">
    <!-- font awesome stylesheet -->
    <link rel="stylesheet" href="{{ url('frontpage/css/all.min.css') }}">
    <!-- AOS stylesheet -->
    <link rel="stylesheet" href="{{ url('frontpage/css/aos.css') }}">
    <!-- Normalize CSS -->
    <link rel="stylesheet" href="{{ url('frontpage/css/normalize.css') }}">
    <!-- Main stylesheet -->
    <link rel="stylesheet" href="{{ url('frontpage/css/style.css') }}">
    <!-- Responsive stylesheet -->
    <link rel="stylesheet" href="{{ url('frontpage/css/responsive.css') }}">

</head>

<body class="home-four-body">
    <main class="home-four overflow-x-hidden">
        <!-- start header -->
        <header class="bg-white header">
            <!-- mobile header nav -->
            <div class="mobile-menu d-block d-md-none">
                <nav>
                    <ul class="menu">
                        <li><a href="#home">Beranda</a>
                        </li>
                        <li><a href="#services">Daerah Pemilihan</a></li>
                        <li><a href="#resume">Riwayat Calon</a></li>
                        <li><a href="#contact">Aspirasi Masyarakat</a>
                        </li>
                        <li><a href="#contact">Kontak</a></li>
                    </ul>
                </nav>
            </div>
            <div class="container d-none d-md-block">
                <div class="header-wrapper d-flex justify-content-between align-items-center">
                    <div class="logo">
                        <a href="https://yuyunhidayat.id/">
                            <img src="{{ url('frontpage/img/logoatas.png') }}" alt="https://yuyunhidayat.id/">
                        </a>
                    </div>
                    <div class="header-menu-wrapper main-menu">
                        <!-- nav menu -->
                        <nav class="desktop-nav">
                            <ul class="header-menu d-flex flex-row justify-content-center menu">
                                <li class="px-2 px-lg-3 ">
                                    <a class="menu-link" href="#home">Beranda</a>
                                </li>
                                <li class="px-2 px-lg-3 ">
                                    <a class="menu-link" href="#services">Daerah Pemilihan</a>
                                </li>

                                <li class="px-2 px-lg-3 ">
                                    <a class="menu-link" href="#resume">Riwayat Calon</a>
                                </li>
                                <li class="px-2 px-lg-3 ">
                                    <a class="menu-link" href="#contact">Aspirasi Masyarakat</a>
                                </li>

                            </ul>
                        </nav>
                    </div>
                    <!-- header button -->
                    <div class="header-btn-wrapper">
                        <a class="btn orange-btn btn_effect" href="#contact">
                            <span class="z-1 position-relative">Kontak</span>
                        </a>
                    </div>
                </div>
            </div>
        </header>
        <!-- end header -->
        <!-- start banner -->
        <section id="home" class="home-four-banner position-relative">
            <div class="container-fluid custom-container">
                <div class="row">
                    <div class="col-12">
                        <div class="home-banner-wrapper row align-items-end">
                            <div class="col-12 col-lg-6 col-xxl-7 home-banner-text-col position-relative">
                                <div class="d-flex align-items-center">
                                    <p class="fw-500 black-color text-uppercase mb-30">Terima Kasih Telah Mengunjungi
                                        Website Ini #BALADYUYUN</p>
                                </div>
                                <div class="animate-heading">
                                    <h1 class="black-color h1-home-4 fw-500 ah-headline">H. Yuyun Hidayat, Drs.<span
                                            class="orange-color fw-700">Menuju <span class="ah-words-wrapper">
                                                <b class="is-visible">Indonesia</b>
                                                <b>Sejahtera</b>
                                            </span></span>
                                        <br />
                                    </h1>
                                </div>
                                <p class="p line-height-10 mt-35 secondary-white fw-400">
                                    Assalamu'alaikum Wr. Wb.
                                    Sampurasun!

                                    Dina raraga Pileg 2024, Bapak <strong>H. Yuyun Hidayat, Drs.</strong> Pupuhu
                                    Pangurus Yayasan Pendidikan Sebelas April (YPSA) Sumedang, anu ngokolakeun
                                    Universitas Sebelas April (UNSAP), tos gilig singkil, kagungan maksad seja jabung
                                    tumalapung, sabda kumapalang, ngiring ngamajengkeun pangwangunan kangge karaharjaan
                                    masyarakat <strong>Sumedang-Majalengka-Subang</strong>, ku jalan nyalon janten
                                    <strong>Anggota DPR RI Dapil Jawa Barat IX ti Partai Persatuan Pembangunan
                                        (17)</strong> nomor urut <strong>3</strong>.
                                    <br />
                                    <br />
                                    Sumangga urang rojong ku sadayana!
                                    <br />
                                    <br />
                                    Prung geura tarung. Bral geura tandang. Padungdung lain rek neangan musuh. Matandang
                                    lain rek neangan lawan. Tapi baris nyungsi gigili takdir kersana Gusti. Braallll,
                                    mugia hasil anu dimaksad. Aamiin.
                                    <br />
                                    <br />
                                    Nuhun.
                                    Wassalamu'alaikum Wr. Wb.

                                </p>
                                <div class="mt-85">
                                    <p class="p black-color fw-500 text-uppercase line-height-3">Daftar Relawan</p>
                                    <div class="mt-1">
                                        <a data-aos="fade-up" data-aos-delay="100" data-aos-duration="1500"
                                            class="btn home-banner-btn orange-btn mt-20 btn_effect aos-init"
                                            href="{{ url('register')}}">
                                            <span class="position-relative z-1">Klik Tombol Ini</span>
                                        </a>
                                    </div>
                                </div>
                                <div class="home-four-banner-spacer"></div>
                            </div>
                            <div
                                class="col-12 col-lg-6 col-xxl-5 text-center position-relative home-four-banner-img-col mb-5 mb-lg-0">
                                <img class="img-fluid home-banner-img" src="{{ url('frontpage/img/homeimage.png') }}"
                                    alt="banner-image">
                                <img class="home-four-banner-figma position-absolute zoom-in-out delay-200"
                                    src="{{ url('frontpage/img/logoppp.png') }}" alt="figma">

                                <img class="home-four-banner-sketch position-absolute zoom-in-out"
                                    src="{{ url('frontpage/img/logoppp.png') }}" alt="experience">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- end home-banner -->

        <!-- start services -->
        <section id="services" class="services">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <div class="services-wrapper">
                            <div class="section-heading-middle">

                                <h2 class="black-color line-height-3 h2 text-uppercase text-center">
                                    Daerah Pemilihan
                                </h2>
                            </div>
                            <div class="service-grid-container mt-50 row-mobile-margin">
                                <div class="service-grid-item text-start">
                                    <img class="service-item-img" width="50%"
                                        src="{{ url('frontpage/img/logo-Subang.png') }}" alt="service-img-two">
                                    <h3 class="h3 fw-500 service-item-heading black-color">Kabupaten Subang</h3>
                                    <p class="p service-item-paragraph secondary-black fw-400 line-height-7">
                                        <!-- Deskripsi -->
                                    </p>
                                </div>
                                <div class="service-grid-item text-start">
                                    <img class="service-item-img" width="50%"
                                        src="{{ url('frontpage/img/logomajalengka.png') }}" alt="service-img-two">
                                    <h3 class="h3 fw-500 service-item-heading black-color">Kabupaten Majalengka</h3>
                                    <p class="p service-item-paragraph secondary-black fw-400 line-height-7">
                                        <!-- Deskripsi -->
                                    </p>
                                </div>
                                <div class="service-grid-item text-start">
                                    <img class="service-item-img" width="50%"
                                        src="{{ url('frontpage/img/logosumedang.png') }}" alt="service-img-two">
                                    <h3 class="h3 fw-500 service-item-heading black-color">Kabupaten Sumedang</h3>
                                    <p class="p service-item-paragraph secondary-black fw-400 line-height-7">
                                        <!-- Deskripsi -->
                                    </p>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- start resume -->
        <section id="resume" class="resume-four">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <div class="resume-four-container">
                            <div class="row">
                                <div class="col-12 col-xl-12 resume-four-right">
                                    <div class="education-experience-container">
                                        <div class="nav nav-tabs d-block border-0">
                                            <ul id="myTab" class="education-experience-nav-tab">
                                                <li class="nav-item w-100">
                                                    <a class="resume-nav-tab-item nav-link active" href="#experience"
                                                        data-bs-toggle="tab">Pengalaman Organisasi</a>
                                                </li>
                                                <li class="nav-item w-100">
                                                    <a class="resume-nav-tab-item nav-link" href="#education"
                                                        data-bs-toggle="tab">Riwayat Pendidikan</a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="tab-content">
                                        <div id="experience" class="resume-tab-contents tab-pane fade show active">
                                            <div
                                                class="resume-tab-item bg-white pt-30 pb-30 pl-30 pr-30 row align-items-center mb-30">
                                                <div class="col-12 col-sm-2">
                                                    <div class="resume-tab-img-container mx-auto mx-sm-0">
                                                        <img style="margin-top:40px;margin-left:10px;"
                                                            src="{{ url('frontpage/img/logoppp.png') }}"
                                                            alt="resume tab image">
                                                    </div>
                                                </div>
                                                <div class="col-12 col-sm-5 text-center text-sm-start">
                                                    <h3
                                                        class="resume-tab-title fw-600 line-height-3 black-color mb-10">
                                                        Pendiri dan bagian dari Yayasan Pendidikan Pangeran Kornel
                                                    </h3>
                                                    <h5 class="fw-400 line-height-6 secondary-black">
                                                    </h5>
                                                </div>
                                                <div class="col-12 col-sm-5 text-center text-sm-end mt-3 mt-sm-0">
                                                    <h4
                                                        class="resume-tab-time orange-color fw-600 line-height-5 mb-10">
                                                        Tahun </h4>
                                                    <h5 class="fw-400 line-height-6 black-color">1978</h5>
                                                </div>
                                            </div>
                                            <div
                                                class="resume-tab-item bg-white pt-30 pb-30 pl-30 pr-30 row align-items-center mb-30">
                                                <div class="col-12 col-sm-2">
                                                    <div class="resume-tab-img-container mx-auto mx-sm-0">
                                                        <img style="margin-top:40px;margin-left:10px;"
                                                            src="{{ url('frontpage/img/logoppp.png') }}"
                                                            alt="resume tab image">
                                                    </div>
                                                </div>
                                                <div class="col-12 col-sm-5 text-center text-sm-start">
                                                    <h3
                                                        class="resume-tab-title fw-600 line-height-3 black-color mb-10">
                                                        Pendiri Yayasan Pendidikan Sebelas April Sumedang
                                                    </h3>
                                                    <h5 class="fw-400 line-height-6 secondary-black">
                                                    </h5>
                                                </div>
                                                <div class="col-12 col-sm-5 text-center text-sm-end mt-3 mt-sm-0">
                                                    <h4
                                                        class="resume-tab-time orange-color fw-600 line-height-5 mb-10">
                                                        Tahun</h4>
                                                    <h5 class="fw-400 line-height-6 black-color">1982 - Sekarang
                                                    </h5>
                                                </div>
                                            </div>
                                            <div
                                                class="resume-tab-item bg-white pt-30 pb-30 pl-30 pr-30 row align-items-center mb-30">
                                                <div class="col-12 col-sm-2">
                                                    <div class="resume-tab-img-container mx-auto mx-sm-0">
                                                        <img style="margin-top:40px;margin-left:10px;"
                                                            src="{{ url('frontpage/img/logoppp.png') }}"
                                                            alt="resume tab image">
                                                    </div>
                                                </div>
                                                <div class="col-12 col-sm-5 text-center text-sm-start">
                                                    <h3
                                                        class="resume-tab-title fw-600 line-height-3 black-color mb-10">
                                                        Departemen Penerangan Kabupaten Sumedang
                                                    </h3>
                                                    <h5 class="fw-400 line-height-6 secondary-black">
                                                    </h5>
                                                </div>
                                                <div class="col-12 col-sm-5 text-center text-sm-end mt-3 mt-sm-0">
                                                    <h4
                                                        class="resume-tab-time orange-color fw-600 line-height-5 mb-10">
                                                        Tahun</h4>
                                                    <h5 class="fw-400 line-height-6 black-color">1982 - 2001
                                                    </h5>
                                                </div>
                                            </div>

                                            <div
                                                class="resume-tab-item bg-white pt-30 pb-30 pl-30 pr-30 row align-items-center mb-30">
                                                <div class="col-12 col-sm-2">
                                                    <div class="resume-tab-img-container mx-auto mx-sm-0">
                                                        <img style="margin-top:40px;margin-left:10px;"
                                                            src="{{ url('frontpage/img/logoppp.png') }}"
                                                            alt="resume tab image">
                                                    </div>
                                                </div>
                                                <div class="col-12 col-sm-5 text-center text-sm-start">
                                                    <h3
                                                        class="resume-tab-title fw-600 line-height-3 black-color mb-10">
                                                        Wakil Kepala SMK Korpri Sumedang
                                                    </h3>
                                                    <h5 class="fw-400 line-height-6 secondary-black">
                                                    </h5>
                                                </div>
                                                <div class="col-12 col-sm-5 text-center text-sm-end mt-3 mt-sm-0">
                                                    <h4
                                                        class="resume-tab-time orange-color fw-600 line-height-5 mb-10">
                                                    </h4>
                                                    <h5 class="fw-400 line-height-6 black-color">
                                                    </h5>
                                                </div>
                                            </div>

                                            <div
                                                class="resume-tab-item bg-white pt-30 pb-30 pl-30 pr-30 row align-items-center mb-30">
                                                <div class="col-12 col-sm-2">
                                                    <div class="resume-tab-img-container mx-auto mx-sm-0">
                                                        <img style="margin-top:40px;margin-left:10px;"
                                                            src="{{ url('frontpage/img/logoppp.png') }}"
                                                            alt="resume tab image">
                                                    </div>
                                                </div>
                                                <div class="col-12 col-sm-5 text-center text-sm-start">
                                                    <h3
                                                        class="resume-tab-title fw-600 line-height-3 black-color mb-10">
                                                        Pembantu Ketua II STKIP Sebelas April
                                                    </h3>
                                                    <h5 class="fw-400 line-height-6 secondary-black">
                                                    </h5>
                                                </div>
                                                <div class="col-12 col-sm-5 text-center text-sm-end mt-3 mt-sm-0">
                                                    <h4
                                                        class="resume-tab-time orange-color fw-600 line-height-5 mb-10">
                                                        Tahun</h4>
                                                    <h5 class="fw-400 line-height-6 black-color">1982 - 2001
                                                    </h5>
                                                </div>
                                            </div>

                                            <div
                                                class="resume-tab-item bg-white pt-30 pb-30 pl-30 pr-30 row align-items-center mb-30">
                                                <div class="col-12 col-sm-2">
                                                    <div class="resume-tab-img-container mx-auto mx-sm-0">
                                                        <img style="margin-top:40px;margin-left:10px;"
                                                            src="{{ url('frontpage/img/logoppp.png') }}"
                                                            alt="resume tab image">
                                                    </div>
                                                </div>
                                                <div class="col-12 col-sm-5 text-center text-sm-start">
                                                    <h3
                                                        class="resume-tab-title fw-600 line-height-3 black-color mb-10">
                                                        Bendahara Yayasan Pendidikan Sebelas April Sumedang
                                                    </h3>
                                                    <h5 class="fw-400 line-height-6 secondary-black">
                                                    </h5>
                                                </div>
                                                <div class="col-12 col-sm-5 text-center text-sm-end mt-3 mt-sm-0">
                                                    <h4
                                                        class="resume-tab-time orange-color fw-600 line-height-5 mb-10">
                                                        Tahun</h4>
                                                    <h5 class="fw-400 line-height-6 black-color">1995 - 2014
                                                    </h5>
                                                </div>
                                            </div>

                                            <div
                                                class="resume-tab-item bg-white pt-30 pb-30 pl-30 pr-30 row align-items-center mb-30">
                                                <div class="col-12 col-sm-2">
                                                    <div class="resume-tab-img-container mx-auto mx-sm-0">
                                                        <img style="margin-top:40px;margin-left:10px;"
                                                            src="{{ url('frontpage/img/logoppp.png') }}"
                                                            alt="resume tab image">
                                                    </div>
                                                </div>
                                                <div class="col-12 col-sm-5 text-center text-sm-start">
                                                    <h3
                                                        class="resume-tab-title fw-600 line-height-3 black-color mb-10">
                                                        Ketua Yayasan Pendidikan Repormasi (YASPRI) Sumedang
                                                    </h3>
                                                    <h5 class="fw-400 line-height-6 secondary-black">
                                                    </h5>
                                                </div>
                                                <div class="col-12 col-sm-5 text-center text-sm-end mt-3 mt-sm-0">
                                                    <h4
                                                        class="resume-tab-time orange-color fw-600 line-height-5 mb-10">
                                                        Tahun</h4>
                                                    <h5 class="fw-400 line-height-6 black-color">1999
                                                    </h5>
                                                </div>
                                            </div>

                                            <div
                                                class="resume-tab-item bg-white pt-30 pb-30 pl-30 pr-30 row align-items-center mb-30">
                                                <div class="col-12 col-sm-2">
                                                    <div class="resume-tab-img-container mx-auto mx-sm-0">
                                                        <img style="margin-top:40px;margin-left:10px;"
                                                            src="{{ url('frontpage/img/logoppp.png') }}"
                                                            alt="resume tab image">
                                                    </div>
                                                </div>
                                                <div class="col-12 col-sm-5 text-center text-sm-start">
                                                    <h3
                                                        class="resume-tab-title fw-600 line-height-3 black-color mb-10">
                                                        Kepala SMK Yaspri
                                                    </h3>
                                                    <h5 class="fw-400 line-height-6 secondary-black">
                                                    </h5>
                                                </div>
                                                <div class="col-12 col-sm-5 text-center text-sm-end mt-3 mt-sm-0">
                                                    <h4
                                                        class="resume-tab-time orange-color fw-600 line-height-5 mb-10">
                                                        Tahun</h4>
                                                    <h5 class="fw-400 line-height-6 black-color">2000
                                                    </h5>
                                                </div>
                                            </div>

                                            <div
                                                class="resume-tab-item bg-white pt-30 pb-30 pl-30 pr-30 row align-items-center mb-30">
                                                <div class="col-12 col-sm-2">
                                                    <div class="resume-tab-img-container mx-auto mx-sm-0">
                                                        <img style="margin-top:40px;margin-left:10px;"
                                                            src="{{ url('frontpage/img/logoppp.png') }}"
                                                            alt="resume tab image">
                                                    </div>
                                                </div>
                                                <div class="col-12 col-sm-5 text-center text-sm-start">
                                                    <h3
                                                        class="resume-tab-title fw-600 line-height-3 black-color mb-10">
                                                        Pendiri SMK YPSA Sumedang
                                                    </h3>
                                                    <h5 class="fw-400 line-height-6 secondary-black">
                                                    </h5>
                                                </div>
                                                <div class="col-12 col-sm-5 text-center text-sm-end mt-3 mt-sm-0">
                                                    <h4
                                                        class="resume-tab-time orange-color fw-600 line-height-5 mb-10">
                                                        Tahun</h4>
                                                    <h5 class="fw-400 line-height-6 black-color">1995
                                                    </h5>
                                                </div>
                                            </div>

                                            <div
                                                class="resume-tab-item bg-white pt-30 pb-30 pl-30 pr-30 row align-items-center mb-30">
                                                <div class="col-12 col-sm-2">
                                                    <div class="resume-tab-img-container mx-auto mx-sm-0">
                                                        <img style="margin-top:40px;margin-left:10px;"
                                                            src="{{ url('frontpage/img/logoppp.png') }}"
                                                            alt="resume tab image">
                                                    </div>
                                                </div>
                                                <div class="col-12 col-sm-5 text-center text-sm-start">
                                                    <h3
                                                        class="resume-tab-title fw-600 line-height-3 black-color mb-10">
                                                        Yayasan Wahana Bakti Majalengka
                                                    </h3>
                                                    <h5 class="fw-400 line-height-6 secondary-black">
                                                    </h5>
                                                </div>
                                                <div class="col-12 col-sm-5 text-center text-sm-end mt-3 mt-sm-0">
                                                    <h4
                                                        class="resume-tab-time orange-color fw-600 line-height-5 mb-10">
                                                        Tahun</h4>
                                                    <h5 class="fw-400 line-height-6 black-color">1989
                                                    </h5>
                                                </div>
                                            </div>

                                            <div
                                                class="resume-tab-item bg-white pt-30 pb-30 pl-30 pr-30 row align-items-center mb-30">
                                                <div class="col-12 col-sm-2">
                                                    <div class="resume-tab-img-container mx-auto mx-sm-0">
                                                        <img style="margin-top:40px;margin-left:10px;"
                                                            src="{{ url('frontpage/img/logoppp.png') }}"
                                                            alt="resume tab image">
                                                    </div>
                                                </div>
                                                <div class="col-12 col-sm-5 text-center text-sm-start">
                                                    <h3
                                                        class="resume-tab-title fw-600 line-height-3 black-color mb-10">
                                                        Pendiri Jurusan Pendidikan Teknik Mesin STKIP Sebelas April
                                                    </h3>
                                                    <h5 class="fw-400 line-height-6 secondary-black">
                                                    </h5>
                                                </div>
                                                <div class="col-12 col-sm-5 text-center text-sm-end mt-3 mt-sm-0">
                                                    <h4
                                                        class="resume-tab-time orange-color fw-600 line-height-5 mb-10">
                                                        Tahun</h4>
                                                    <h5 class="fw-400 line-height-6 black-color">2011
                                                    </h5>
                                                </div>
                                            </div>

                                            <div
                                                class="resume-tab-item bg-white pt-30 pb-30 pl-30 pr-30 row align-items-center mb-30">
                                                <div class="col-12 col-sm-2">
                                                    <div class="resume-tab-img-container mx-auto mx-sm-0">
                                                        <img style="margin-top:40px;margin-left:10px;"
                                                            src="{{ url('frontpage/img/logoppp.png') }}"
                                                            alt="resume tab image">
                                                    </div>
                                                </div>
                                                <div class="col-12 col-sm-5 text-center text-sm-start">
                                                    <h3
                                                        class="resume-tab-title fw-600 line-height-3 black-color mb-10">
                                                        Pendiri Jurusan Pendidikan Guru Sekolah Dasar STKIP Sebelas
                                                        April
                                                    </h3>
                                                    <h5 class="fw-400 line-height-6 secondary-black">
                                                    </h5>
                                                </div>
                                                <div class="col-12 col-sm-5 text-center text-sm-end mt-3 mt-sm-0">
                                                    <h4
                                                        class="resume-tab-time orange-color fw-600 line-height-5 mb-10">
                                                        Tahun</h4>
                                                    <h5 class="fw-400 line-height-6 black-color">2011
                                                    </h5>
                                                </div>
                                            </div>

                                            <div
                                                class="resume-tab-item bg-white pt-30 pb-30 pl-30 pr-30 row align-items-center mb-30">
                                                <div class="col-12 col-sm-2">
                                                    <div class="resume-tab-img-container mx-auto mx-sm-0">
                                                        <img style="margin-top:40px;margin-left:10px;"
                                                            src="{{ url('frontpage/img/logoppp.png') }}"
                                                            alt="resume tab image">
                                                    </div>
                                                </div>
                                                <div class="col-12 col-sm-5 text-center text-sm-start">
                                                    <h3
                                                        class="resume-tab-title fw-600 line-height-3 black-color mb-10">
                                                        Pendiri Universitas Sebelas April
                                                    </h3>
                                                    <h5 class="fw-400 line-height-6 secondary-black">
                                                    </h5>
                                                </div>
                                                <div class="col-12 col-sm-5 text-center text-sm-end mt-3 mt-sm-0">
                                                    <h4
                                                        class="resume-tab-time orange-color fw-600 line-height-5 mb-10">
                                                        Tahun</h4>
                                                    <h5 class="fw-400 line-height-6 black-color">2021
                                                    </h5>
                                                </div>
                                            </div>

                                            <div
                                                class="resume-tab-item bg-white pt-30 pb-30 pl-30 pr-30 row align-items-center mb-30">
                                                <div class="col-12 col-sm-2">
                                                    <div class="resume-tab-img-container mx-auto mx-sm-0">
                                                        <img style="margin-top:40px;margin-left:10px;"
                                                            src="{{ url('frontpage/img/logoppp.png') }}"
                                                            alt="resume tab image">
                                                    </div>
                                                </div>
                                                <div class="col-12 col-sm-5 text-center text-sm-start">
                                                    <h3
                                                        class="resume-tab-title fw-600 line-height-3 black-color mb-10">
                                                        Dewan Penyantun Cabang Olahraga Basket dan Petang
                                                    </h3>
                                                    <h5 class="fw-400 line-height-6 secondary-black">
                                                    </h5>
                                                </div>
                                                <div class="col-12 col-sm-5 text-center text-sm-end mt-3 mt-sm-0">
                                                    <h4
                                                        class="resume-tab-time orange-color fw-600 line-height-5 mb-10">
                                                        Tahun</h4>
                                                    <h5 class="fw-400 line-height-6 black-color">2018
                                                    </h5>
                                                </div>
                                            </div>

                                            {{--   END --}}

                                        </div>

                                        <div id="education" class="resume-tab-contents tab-pane fade">
                                            <div
                                                class="resume-tab-item bg-white pt-30 pb-30 pl-30 pr-30 row align-items-center mb-30">
                                                <div class="col-12 col-sm-2">
                                                    <div class="resume-tab-img-container mx-auto mx-sm-0">
                                                        <img style="margin-top:40px;margin-left:10px;"
                                                            src="{{ url('frontpage/img/logoppp.png') }}"
                                                            alt="resume tab image">
                                                    </div>
                                                </div>
                                                <div class="col-12 col-sm-5 text-center text-sm-start">
                                                    <h3
                                                        class="resume-tab-title fw-600 line-height-3 black-color mb-10">
                                                        Sarjana (S1) Universitas Islam Nusantara
                                                    </h3>
                                                    <h5 class="fw-400 line-height-6 secondary-black">UNINUS
                                                    </h5>
                                                </div>
                                                <div class="col-12 col-sm-5 text-center text-sm-end mt-3 mt-sm-0">
                                                    <h4
                                                        class="resume-tab-time orange-color fw-600 line-height-5 mb-10">
                                                        Tahun</h4>
                                                    <h5 class="fw-400 line-height-6 black-color">1985
                                                    </h5>
                                                </div>
                                            </div>
                                            <div
                                                class="resume-tab-item bg-white pt-30 pb-30 pl-30 pr-30 row align-items-center mb-30">
                                                <div class="col-12 col-sm-2">
                                                    <div class="resume-tab-img-container mx-auto mx-sm-0">
                                                        <img style="margin-top:40px;margin-left:10px;"
                                                            src="{{ url('frontpage/img/logoppp.png') }}"
                                                            alt="resume tab image">
                                                    </div>
                                                </div>
                                                <div class="col-12 col-sm-5 text-center text-sm-start">
                                                    <h3
                                                        class="resume-tab-title fw-600 line-height-3 black-color mb-10">
                                                        Sekolah Teknik Mesin Negeri (STMN)
                                                    </h3>
                                                    <h5 class="fw-400 line-height-6 secondary-black">STMN SUMEDANG
                                                    </h5>
                                                </div>
                                                <div class="col-12 col-sm-5 text-center text-sm-end mt-3 mt-sm-0">
                                                    <h4
                                                        class="resume-tab-time orange-color fw-600 line-height-5 mb-10">
                                                        Tahun</h4>
                                                    <h5 class="fw-400 line-height-6 black-color">1973</h5>
                                                </div>
                                            </div>
                                            <div
                                                class="resume-tab-item bg-white pt-30 pb-30 pl-30 pr-30 row align-items-center mb-30">
                                                <div class="col-12 col-sm-2">
                                                    <div class="resume-tab-img-container mx-auto mx-sm-0">
                                                        <img style="margin-top:40px;margin-left:10px;"
                                                            src="{{ url('frontpage/img/logoppp.png') }}"
                                                            alt="resume tab image">
                                                    </div>
                                                </div>
                                                <div class="col-12 col-sm-5 text-center text-sm-start">
                                                    <h3
                                                        class="resume-tab-title fw-600 line-height-3 black-color mb-10">
                                                        Sekolah Menengah Pertama Negeri (SMPN) 3 Sumedang
                                                    </h3>
                                                    <h5 class="fw-400 line-height-6 secondary-black">SMPN 3 Sumedang
                                                    </h5>
                                                </div>
                                                <div class="col-12 col-sm-5 text-center text-sm-end mt-3 mt-sm-0">
                                                    <h4
                                                        class="resume-tab-time orange-color fw-600 line-height-5 mb-10">
                                                        Tahun</h4>
                                                    <h5 class="fw-400 line-height-6 black-color">1969
                                                    </h5>
                                                </div>
                                            </div>

                                            <div
                                                class="resume-tab-item bg-white pt-30 pb-30 pl-30 pr-30 row align-items-center mb-30">
                                                <div class="col-12 col-sm-2">
                                                    <div class="resume-tab-img-container mx-auto mx-sm-0">
                                                        <img style="margin-top:40px;margin-left:10px;"
                                                            src="{{ url('frontpage/img/logoppp.png') }}"
                                                            alt="resume tab image">
                                                    </div>
                                                </div>
                                                <div class="col-12 col-sm-5 text-center text-sm-start">
                                                    <h3
                                                        class="resume-tab-title fw-600 line-height-3 black-color mb-10">
                                                        Sekolah Dasar Negeri (SDN) Ganeas
                                                    </h3>
                                                    <h5 class="fw-400 line-height-6 secondary-black">SDN GANEAS
                                                    </h5>
                                                </div>
                                                <div class="col-12 col-sm-5 text-center text-sm-end mt-3 mt-sm-0">
                                                    <h4
                                                        class="resume-tab-time orange-color fw-600 line-height-5 mb-10">
                                                        Tahun</h4>
                                                    <h5 class="fw-400 line-height-6 black-color">1966
                                                    </h5>
                                                </div>
                                            </div>

                                            {{-- END --}}

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- end resume -->
        <!-- start portfolio gallery -->
        <section id="portfolio" class="portfolio-two">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <div class="portfolio-wrapper">
                            <div class="section-heading-middle">
                                <div class="sub-heading d-flex align-items-center mx-auto">
                                    <img src="{{ url('frontpage/img/orangeDot.png') }}" alt="orange-dot">
                                    <p>#BALADYUYUN</p>
                                </div>
                                <h2 class="black-color line-height-3 h2 text-uppercase text-center">
                                    GALLERY FOTO H. YUYUN HIDAYAT, Drs.
                                </h2>
                            </div>
                            <div class="mt-50 row-mobile-margin">
                                <div class="controls d-flex justify-content-center flex-wrap gap-1 gap-lg-4 mb-45">
                                    <button type="button"
                                        class="control filter btn .outline-0 border-0 secondary-black"
                                        data-filter=".all">Semua Kegiatan</button>
                                    <button type="button"
                                        class="control filter btn .outline-0 border-0 secondary-black"
                                        data-filter=".pendidikan">Kegiatan Pendidikan</button>
                                    <button type="button"
                                        class="control filter btn .outline-0 border-0 secondary-black"
                                        data-filter=".sosial">Kegiatan Sosial</button>
                                    <button type="button"
                                        class="control filter btn .outline-0 border-0 secondary-black"
                                        data-filter=".keluarga">Keluarga</button>

                                </div>
                                <div class="portfolio-massonary-container">
                                
                                    <div class="portfolio-massonary-four-items mix pendidikan all position-relative">
                                        <div class="modal fade" id="recentModalSix" tabindex="-1"
                                            aria-hidden="true">
                                            <div class="modal-dialog modal-dialog-centered justify-content-center">
                                                <div>
                                                    <button type="button" class="btn-close mb-10 btn"
                                                        data-bs-dismiss="modal" aria-label="Close"></button>
                                                    <img class="img-fluid"
                                                        src="{{ url('images/pendidikan/1.jpeg') }}"
                                                        alt="portfolio img">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="img-overlay-full">
                                            <img class="img-fluid" src="{{ url('images/pendidikan/1.jpeg') }}"
                                                alt="portfolio img">
                                        </div>
                                        <div class="w-100 justify-content-center align-items-center pt-15">
                                            <p class="secondary-black fw-400 line-height-7 text-uppercase"><!-- Judul
                                                Kegiatan --></p>
                                            <a data-bs-toggle="modal" data-bs-target="#recentModalSix"
                                                href="#recentModalSix"
                                                class="h4 black-color fw-600 line-height-3 portfolio-massonary-item-link">
                                                <!-- Deskripsi Kegiatan -->
                                            </a>
                                        </div>
                                    </div>
                                    <div class="portfolio-massonary-four-items mix pendidikan all position-relative">
                                        <div class="modal fade" id="recentModalSix" tabindex="-1"
                                            aria-hidden="true">
                                            <div class="modal-dialog modal-dialog-centered justify-content-center">
                                                <div>
                                                    <button type="button" class="btn-close mb-10 btn"
                                                        data-bs-dismiss="modal" aria-label="Close"></button>
                                                    <img class="img-fluid"
                                                        src="{{ url('images/pendidikan/bayu.png') }}"
                                                        alt="portfolio img">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="img-overlay-full">
                                            <img class="img-fluid" src="{{ url('images/pendidikan/bayu.png') }}"
                                                alt="portfolio img">
                                        </div>
                                        <div class="w-100 justify-content-center align-items-center pt-15">
                                            <p class="secondary-black fw-400 line-height-7 text-uppercase"><!-- Judul
                                                Kegiatan --></p>
                                            <a data-bs-toggle="modal" data-bs-target="#recentModalSix"
                                                href="#recentModalSix"
                                                class="h4 black-color fw-600 line-height-3 portfolio-massonary-item-link">
                                                <!-- Deskripsi Kegiatan -->
                                            </a>
                                        </div>
                                    </div>
                                    <div class="portfolio-massonary-four-items mix pendidikan all position-relative">
                                        <div class="modal fade" id="recentModalEight" tabindex="-1"
                                            aria-hidden="true">
                                            <div class="modal-dialog modal-dialog-centered justify-content-center">
                                                <div>
                                                    <button type="button" class="btn-close mb-10 btn"
                                                        data-bs-dismiss="modal" aria-label="Close"></button>
                                                    <img class="img-fluid" src="{{ url('images/pendidikan/4.JPG') }}"
                                                        alt="portfolio img">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="img-overlay-full">
                                            <img class="img-fluid" src="{{ url('images/pendidikan/4.JPG') }}"
                                                alt="portfolio img">
                                        </div>
                                        <div class="w-100 justify-content-center align-items-center pt-15">
                                            <p class="secondary-black fw-400 line-height-7 text-uppercase"><!-- Judul
                                                Kegiatan --></p>
                                            <a data-bs-toggle="modal" data-bs-target="#recentModalEight"
                                                href="#recentModalEight"
                                                class="h4 black-color fw-600 line-height-3 portfolio-massonary-item-link">
                                                <!-- Deskripsi Kegiatan -->
                                            </a>
                                        </div>
                                    </div>

                                    <div class="portfolio-massonary-four-items mix pendidikan all position-relative">
                                        <div class="modal fade" id="recentModalEight" tabindex="-1"
                                            aria-hidden="true">
                                            <div class="modal-dialog modal-dialog-centered justify-content-center">
                                                <div>
                                                    <button type="button" class="btn-close mb-10 btn"
                                                        data-bs-dismiss="modal" aria-label="Close"></button>
                                                    <img class="img-fluid" src="{{ url('images/pendidikan/5.JPG') }}"
                                                        alt="portfolio img">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="img-overlay-full">
                                            <img class="img-fluid" src="{{ url('images/pendidikan/5.JPG') }}"
                                                alt="portfolio img">
                                        </div>
                                        <div class="w-100 justify-content-center align-items-center pt-15">
                                            <p class="secondary-black fw-400 line-height-7 text-uppercase"><!-- Judul
                                                Kegiatan --></p>
                                            <a data-bs-toggle="modal" data-bs-target="#recentModalEight"
                                                href="#recentModalEight"
                                                class="h4 black-color fw-600 line-height-3 portfolio-massonary-item-link">
                                                <!-- Deskripsi Kegiatan -->
                                            </a>
                                        </div>
                                    </div>

                                    <div class="portfolio-massonary-four-items mix pendidikan all position-relative">
                                        <div class="modal fade" id="recentModalNine" tabindex="-1"
                                            aria-hidden="true">
                                            <div class="modal-dialog modal-dialog-centered justify-content-center">
                                                <div>
                                                    <button type="button" class="btn-close mb-10 btn"
                                                        data-bs-dismiss="modal" aria-label="Close"></button>
                                                    <img class="img-fluid" src="{{ url('images/pendidikan/6.JPG') }}"
                                                        alt="portfolio img">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="img-overlay-full">
                                            <img class="img-fluid" src="{{ url('images/pendidikan/6.JPG') }}"
                                                alt="portfolio img">
                                        </div>
                                        <div class="w-100 justify-content-center align-items-center pt-15">
                                            <p class="secondary-black fw-400 line-height-7 text-uppercase"><!-- Judul
                                                Kegiatan --></p>
                                            <a data-bs-toggle="modal" data-bs-target="#recentModalNine"
                                                href="#recentModalNine"
                                                class="h4 black-color fw-600 line-height-3 portfolio-massonary-item-link">
                                                <!-- Deskripsi Kegiatan -->
                                            </a>
                                        </div>
                                    </div>

                                    {{--  START --}}
                                    <div class="portfolio-massonary-four-items mix pendidikan all position-relative">
                                        <div class="modal fade" id="recentModalTen" tabindex="-1"
                                            aria-hidden="true">
                                            <div class="modal-dialog modal-dialog-centered justify-content-center">
                                                <div>
                                                    <button type="button" class="btn-close mb-10 btn"
                                                        data-bs-dismiss="modal" aria-label="Close"></button>
                                                    <img class="img-fluid" src="{{ url('images/pendidikan/7.JPG') }}"
                                                        alt="portfolio img">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="img-overlay-full">
                                            <img class="img-fluid" src="{{ url('images/pendidikan/7.JPG') }}"
                                                alt="portfolio img">
                                        </div>
                                        <div class="w-100 justify-content-center align-items-center pt-15">
                                            <p class="secondary-black fw-400 line-height-7 text-uppercase"><!-- Judul
                                                Kegiatan --></p>
                                            <a data-bs-toggle="modal" data-bs-target="#recentModalTen"
                                                href="#recentModalTen"
                                                class="h4 black-color fw-600 line-height-3 portfolio-massonary-item-link">
                                                <!-- Deskripsi Kegiatan -->
                                            </a>
                                        </div>
                                    </div>
                                    {{--  EnD --}}

                                    {{--  START --}}
                                    <div class="portfolio-massonary-four-items mix pendidikan all position-relative">
                                        <div class="modal fade" id="recentModalTen" tabindex="-1"
                                            aria-hidden="true">
                                            <div class="modal-dialog modal-dialog-centered justify-content-center">
                                                <div>
                                                    <button type="button" class="btn-close mb-10 btn"
                                                        data-bs-dismiss="modal" aria-label="Close"></button>
                                                    <img class="img-fluid" src="{{ url('images/pendidikan/8.JPG') }}"
                                                        alt="portfolio img">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="img-overlay-full">
                                            <img class="img-fluid" src="{{ url('images/pendidikan/8.JPG') }}"
                                                alt="portfolio img">
                                        </div>
                                        <div class="w-100 justify-content-center align-items-center pt-15">
                                            <p class="secondary-black fw-400 line-height-7 text-uppercase"><!-- Judul
                                                Kegiatan --></p>
                                            <a data-bs-toggle="modal" data-bs-target="#recentModalTen"
                                                href="#recentModalTen"
                                                class="h4 black-color fw-600 line-height-3 portfolio-massonary-item-link">
                                                <!-- Deskripsi Kegiatan -->
                                            </a>
                                        </div>
                                    </div>
                                    {{--  EnD --}}

                                    {{--  START --}}
                                    <div class="portfolio-massonary-four-items mix pendidikan all position-relative">
                                        <div class="modal fade" id="recentModalTen" tabindex="-1"
                                            aria-hidden="true">
                                            <div class="modal-dialog modal-dialog-centered justify-content-center">
                                                <div>
                                                    <button type="button" class="btn-close mb-10 btn"
                                                        data-bs-dismiss="modal" aria-label="Close"></button>
                                                    <img class="img-fluid" src="{{ url('images/pendidikan/9.JPG') }}"
                                                        alt="portfolio img">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="img-overlay-full">
                                            <img class="img-fluid" src="{{ url('images/pendidikan/9.JPG') }}"
                                                alt="portfolio img">
                                        </div>
                                        <div class="w-100 justify-content-center align-items-center pt-15">
                                            <p class="secondary-black fw-400 line-height-7 text-uppercase"><!-- Judul
                                                Kegiatan --></p>
                                            <a data-bs-toggle="modal" data-bs-target="#recentModalTen"
                                                href="#recentModalTen"
                                                class="h4 black-color fw-600 line-height-3 portfolio-massonary-item-link">
                                                <!-- Deskripsi Kegiatan -->
                                            </a>
                                        </div>
                                    </div>
                                    {{--  EnD --}}

                                    {{--  START --}}
                                    <div class="portfolio-massonary-four-items mix pendidikan all position-relative">
                                        <div class="modal fade" id="recentModalTen" tabindex="-1"
                                            aria-hidden="true">
                                            <div class="modal-dialog modal-dialog-centered justify-content-center">
                                                <div>
                                                    <button type="button" class="btn-close mb-10 btn"
                                                        data-bs-dismiss="modal" aria-label="Close"></button>
                                                    <img class="img-fluid" src="{{ url('images/pendidikan/10.JPG') }}"
                                                        alt="portfolio img">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="img-overlay-full">
                                            <img class="img-fluid" src="{{ url('images/pendidikan/10.JPG') }}"
                                                alt="portfolio img">
                                        </div>
                                        <div class="w-100 justify-content-center align-items-center pt-15">
                                            <p class="secondary-black fw-400 line-height-7 text-uppercase"><!-- Judul
                                                Kegiatan --></p>
                                            <a data-bs-toggle="modal" data-bs-target="#recentModalTen"
                                                href="#recentModalTen"
                                                class="h4 black-color fw-600 line-height-3 portfolio-massonary-item-link">
                                                <!-- Deskripsi Kegiatan -->
                                            </a>
                                        </div>
                                    </div>
                                    {{--  EnD --}}
                                    {{--  START --}}
                                    <div class="portfolio-massonary-four-items mix pendidikan all position-relative">
                                        <div class="modal fade" id="recentModalTen" tabindex="-1"
                                            aria-hidden="true">
                                            <div class="modal-dialog modal-dialog-centered justify-content-center">
                                                <div>
                                                    <button type="button" class="btn-close mb-10 btn"
                                                        data-bs-dismiss="modal" aria-label="Close"></button>
                                                    <img class="img-fluid" src="{{ url('images/pendidikan/11.JPG') }}"
                                                        alt="portfolio img">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="img-overlay-full">
                                            <img class="img-fluid" src="{{ url('images/pendidikan/11.JPG') }}"
                                                alt="portfolio img">
                                        </div>
                                        <div class="w-100 justify-content-center align-items-center pt-15">
                                            <p class="secondary-black fw-400 line-height-7 text-uppercase"><!-- Judul
                                                Kegiatan --></p>
                                            <a data-bs-toggle="modal" data-bs-target="#recentModalTen"
                                                href="#recentModalTen"
                                                class="h4 black-color fw-600 line-height-3 portfolio-massonary-item-link">
                                                <!-- Deskripsi Kegiatan -->
                                            </a>
                                        </div>
                                    </div>
                                    {{--  EnD --}}

                                    {{--  START --}}
                                    <div class="portfolio-massonary-four-items mix pendidikan all position-relative">
                                        <div class="modal fade" id="recentModalTen" tabindex="-1"
                                            aria-hidden="true">
                                            <div class="modal-dialog modal-dialog-centered justify-content-center">
                                                <div>
                                                    <button type="button" class="btn-close mb-10 btn"
                                                        data-bs-dismiss="modal" aria-label="Close"></button>
                                                    <img class="img-fluid" src="{{ url('images/pendidikan/12.JPG') }}"
                                                        alt="portfolio img">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="img-overlay-full">
                                            <img class="img-fluid" src="{{ url('images/pendidikan/12.JPG') }}"
                                                alt="portfolio img">
                                        </div>
                                        <div class="w-100 justify-content-center align-items-center pt-15">
                                            <p class="secondary-black fw-400 line-height-7 text-uppercase"><!-- Judul
                                                Kegiatan --></p>
                                            <a data-bs-toggle="modal" data-bs-target="#recentModalTen"
                                                href="#recentModalTen"
                                                class="h4 black-color fw-600 line-height-3 portfolio-massonary-item-link">
                                                <!-- Deskripsi Kegiatan -->
                                            </a>
                                        </div>
                                    </div>
                                    {{--  EnD --}}

                                    {{--  START --}}
                                    <div class="portfolio-massonary-four-items mix pendidikan all position-relative">
                                        <div class="modal fade" id="recentModalTen" tabindex="-1"
                                            aria-hidden="true">
                                            <div class="modal-dialog modal-dialog-centered justify-content-center">
                                                <div>
                                                    <button type="button" class="btn-close mb-10 btn"
                                                        data-bs-dismiss="modal" aria-label="Close"></button>
                                                    <img class="img-fluid" src="{{ url('images/pendidikan/13.JPG') }}"
                                                        alt="portfolio img">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="img-overlay-full">
                                            <img class="img-fluid" src="{{ url('images/pendidikan/13.JPG') }}"
                                                alt="portfolio img">
                                        </div>
                                        <div class="w-100 justify-content-center align-items-center pt-15">
                                            <p class="secondary-black fw-400 line-height-7 text-uppercase"><!-- Judul
                                                Kegiatan --></p>
                                            <a data-bs-toggle="modal" data-bs-target="#recentModalTen"
                                                href="#recentModalTen"
                                                class="h4 black-color fw-600 line-height-3 portfolio-massonary-item-link">
                                                <!-- Deskripsi Kegiatan -->
                                            </a>
                                        </div>
                                    </div>
                                    {{--  EnD --}}

                                    {{--  START --}}
                                    <div class="portfolio-massonary-four-items mix pendidikan all position-relative">
                                        <div class="modal fade" id="recentModalTen" tabindex="-1"
                                            aria-hidden="true">
                                            <div class="modal-dialog modal-dialog-centered justify-content-center">
                                                <div>
                                                    <button type="button" class="btn-close mb-10 btn"
                                                        data-bs-dismiss="modal" aria-label="Close"></button>
                                                    <img class="img-fluid" src="{{ url('images/pendidikan/14.JPG') }}"
                                                        alt="portfolio img">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="img-overlay-full">
                                            <img class="img-fluid" src="{{ url('images/pendidikan/14.JPG') }}"
                                                alt="portfolio img">
                                        </div>
                                        <div class="w-100 justify-content-center align-items-center pt-15">
                                            <p class="secondary-black fw-400 line-height-7 text-uppercase"><!-- Judul
                                                Kegiatan --></p>
                                            <a data-bs-toggle="modal" data-bs-target="#recentModalTen"
                                                href="#recentModalTen"
                                                class="h4 black-color fw-600 line-height-3 portfolio-massonary-item-link">
                                                <!-- Deskripsi Kegiatan -->
                                            </a>
                                        </div>
                                    </div>
                                    {{--  EnD --}}
                                    {{--  START --}}
                                    <div class="portfolio-massonary-four-items mix pendidikan all position-relative">
                                        <div class="modal fade" id="recentModalTen" tabindex="-1"
                                            aria-hidden="true">
                                            <div class="modal-dialog modal-dialog-centered justify-content-center">
                                                <div>
                                                    <button type="button" class="btn-close mb-10 btn"
                                                        data-bs-dismiss="modal" aria-label="Close"></button>
                                                    <img class="img-fluid" src="{{ url('images/pendidikan/15.JPG') }}"
                                                        alt="portfolio img">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="img-overlay-full">
                                            <img class="img-fluid" src="{{ url('images/pendidikan/15.JPG') }}"
                                                alt="portfolio img">
                                        </div>
                                        <div class="w-100 justify-content-center align-items-center pt-15">
                                            <p class="secondary-black fw-400 line-height-7 text-uppercase"><!-- Judul
                                                Kegiatan --></p>
                                            <a data-bs-toggle="modal" data-bs-target="#recentModalTen"
                                                href="#recentModalTen"
                                                class="h4 black-color fw-600 line-height-3 portfolio-massonary-item-link">
                                                <!-- Deskripsi Kegiatan -->
                                            </a>
                                        </div>
                                    </div>
                                    {{--  EnD --}}
                                    {{--  START --}}
                                    <div class="portfolio-massonary-four-items mix pendidikan all position-relative">
                                        <div class="modal fade" id="recentModalTen" tabindex="-1"
                                            aria-hidden="true">
                                            <div class="modal-dialog modal-dialog-centered justify-content-center">
                                                <div>
                                                    <button type="button" class="btn-close mb-10 btn"
                                                        data-bs-dismiss="modal" aria-label="Close"></button>
                                                    <img class="img-fluid" src="{{ url('images/pendidikan/16.JPG') }}"
                                                        alt="portfolio img">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="img-overlay-full">
                                            <img class="img-fluid" src="{{ url('images/pendidikan/16.JPG') }}"
                                                alt="portfolio img">
                                        </div>
                                        <div class="w-100 justify-content-center align-items-center pt-15">
                                            <p class="secondary-black fw-400 line-height-7 text-uppercase"><!-- Judul
                                                Kegiatan --></p>
                                            <a data-bs-toggle="modal" data-bs-target="#recentModalTen"
                                                href="#recentModalTen"
                                                class="h4 black-color fw-600 line-height-3 portfolio-massonary-item-link">
                                                <!-- Deskripsi Kegiatan -->
                                            </a>
                                        </div>
                                    </div>
                                    {{--  EnD --}}
                                    {{--  START --}}
                                    <div class="portfolio-massonary-four-items mix pendidikan all position-relative">
                                        <div class="modal fade" id="recentModalTen" tabindex="-1"
                                            aria-hidden="true">
                                            <div class="modal-dialog modal-dialog-centered justify-content-center">
                                                <div>
                                                    <button type="button" class="btn-close mb-10 btn"
                                                        data-bs-dismiss="modal" aria-label="Close"></button>
                                                    <img class="img-fluid" src="{{ url('images/pendidikan/17.JPG') }}"
                                                        alt="portfolio img">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="img-overlay-full">
                                            <img class="img-fluid" src="{{ url('images/pendidikan/17.JPG') }}"
                                                alt="portfolio img">
                                        </div>
                                        <div class="w-100 justify-content-center align-items-center pt-15">
                                            <p class="secondary-black fw-400 line-height-7 text-uppercase"><!-- Judul
                                                Kegiatan --></p>
                                            <a data-bs-toggle="modal" data-bs-target="#recentModalTen"
                                                href="#recentModalTen"
                                                class="h4 black-color fw-600 line-height-3 portfolio-massonary-item-link">
                                                <!-- Deskripsi Kegiatan -->
                                            </a>
                                        </div>
                                    </div>
                                    {{--  EnD --}}

                                    {{--  START --}}
                                    <div class="portfolio-massonary-four-items mix pendidikan all position-relative">
                                        <div class="modal fade" id="recentModalTen" tabindex="-1"
                                            aria-hidden="true">
                                            <div class="modal-dialog modal-dialog-centered justify-content-center">
                                                <div>
                                                    <button type="button" class="btn-close mb-10 btn"
                                                        data-bs-dismiss="modal" aria-label="Close"></button>
                                                    <img class="img-fluid" src="{{ url('images/pendidikan/18.JPG') }}"
                                                        alt="portfolio img">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="img-overlay-full">
                                            <img class="img-fluid" src="{{ url('images/pendidikan/18.JPG') }}"
                                                alt="portfolio img">
                                        </div>
                                        <div class="w-100 justify-content-center align-items-center pt-15">
                                            <p class="secondary-black fw-400 line-height-7 text-uppercase"><!-- Judul
                                                Kegiatan --></p>
                                            <a data-bs-toggle="modal" data-bs-target="#recentModalTen"
                                                href="#recentModalTen"
                                                class="h4 black-color fw-600 line-height-3 portfolio-massonary-item-link">
                                                <!-- Deskripsi Kegiatan -->
                                            </a>
                                        </div>
                                    </div>
                                    {{--  EnD --}}
                                    {{--  START --}}
                                    <div class="portfolio-massonary-four-items mix pendidikan all position-relative">
                                        <div class="modal fade" id="recentModalTen" tabindex="-1"
                                            aria-hidden="true">
                                            <div class="modal-dialog modal-dialog-centered justify-content-center">
                                                <div>
                                                    <button type="button" class="btn-close mb-10 btn"
                                                        data-bs-dismiss="modal" aria-label="Close"></button>
                                                    <img class="img-fluid" src="{{ url('images/pendidikan/19.JPG') }}"
                                                        alt="portfolio img">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="img-overlay-full">
                                            <img class="img-fluid" src="{{ url('images/pendidikan/19.JPG') }}"
                                                alt="portfolio img">
                                        </div>
                                        <div class="w-100 justify-content-center align-items-center pt-15">
                                            <p class="secondary-black fw-400 line-height-7 text-uppercase"><!-- Judul
                                                Kegiatan --></p>
                                            <a data-bs-toggle="modal" data-bs-target="#recentModalTen"
                                                href="#recentModalTen"
                                                class="h4 black-color fw-600 line-height-3 portfolio-massonary-item-link">
                                                <!-- Deskripsi Kegiatan -->
                                            </a>
                                        </div>
                                    </div>
                                    {{--  EnD --}}

                                    {{--  START --}}
                                    <div class="portfolio-massonary-four-items mix sosial all position-relative">
                                        <div class="modal fade" id="recentModalTen" tabindex="-1"
                                            aria-hidden="true">
                                            <div class="modal-dialog modal-dialog-centered justify-content-center">
                                                <div>
                                                    <button type="button" class="btn-close mb-10 btn"
                                                        data-bs-dismiss="modal" aria-label="Close"></button>
                                                    <img class="img-fluid" src="{{ url('images/sosial/1.jpeg') }}"
                                                        alt="portfolio img">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="img-overlay-full">
                                            <img class="img-fluid" src="{{ url('images/sosial/1.jpeg') }}"
                                                alt="portfolio img">
                                        </div>
                                        <div class="w-100 justify-content-center align-items-center pt-15">
                                            <p class="secondary-black fw-400 line-height-7 text-uppercase"><!-- Judul
                                                Kegiatan --></p>
                                            <a data-bs-toggle="modal" data-bs-target="#recentModalTen"
                                                href="#recentModalTen"
                                                class="h4 black-color fw-600 line-height-3 portfolio-massonary-item-link">
                                                <!-- Deskripsi Kegiatan -->
                                            </a>
                                        </div>
                                    </div>
                                    <div class="portfolio-massonary-four-items mix sosial all position-relative">
                                        <div class="modal fade" id="recentModalSix" tabindex="-1"
                                            aria-hidden="true">
                                            <div class="modal-dialog modal-dialog-centered justify-content-center">
                                                <div>
                                                    <button type="button" class="btn-close mb-10 btn"
                                                        data-bs-dismiss="modal" aria-label="Close"></button>
                                                    <img class="img-fluid"
                                                        src="{{ url('images/sosial/4.jpeg') }}"
                                                        alt="portfolio img">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="img-overlay-full">
                                            <img class="img-fluid" src="{{ url('images/sosial/4.jpeg') }}"
                                                alt="portfolio img">
                                        </div>
                                        <div class="w-100 justify-content-center align-items-center pt-15">
                                            <p class="secondary-black fw-400 line-height-7 text-uppercase"><!-- Judul
                                                Kegiatan --></p>
                                            <a data-bs-toggle="modal" data-bs-target="#recentModalSix"
                                                href="#recentModalSix"
                                                class="h4 black-color fw-600 line-height-3 portfolio-massonary-item-link">
                                                <!-- Deskripsi Kegiatan -->
                                            </a>
                                        </div>
                                    </div>
                                    {{--  EnD --}}

                                    {{--  START --}}
                                    <div class="portfolio-massonary-four-items mix sosial all position-relative">
                                        <div class="modal fade" id="recentModalTen" tabindex="-1"
                                            aria-hidden="true">
                                            <div class="modal-dialog modal-dialog-centered justify-content-center">
                                                <div>
                                                    <button type="button" class="btn-close mb-10 btn"
                                                        data-bs-dismiss="modal" aria-label="Close"></button>
                                                    <img class="img-fluid" src="{{ url('images/sosial/2.jpeg') }}"
                                                        alt="portfolio img">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="img-overlay-full">
                                            <img class="img-fluid" src="{{ url('images/sosial/2.jpeg') }}"
                                                alt="portfolio img">
                                        </div>
                                        <div class="w-100 justify-content-center align-items-center pt-15">
                                            <p class="secondary-black fw-400 line-height-7 text-uppercase"><!-- Judul
                                                Kegiatan --></p>
                                            <a data-bs-toggle="modal" data-bs-target="#recentModalTen"
                                                href="#recentModalTen"
                                                class="h4 black-color fw-600 line-height-3 portfolio-massonary-item-link">
                                                <!-- Deskripsi Kegiatan -->
                                            </a>
                                        </div>
                                    </div>
                                    {{--  EnD --}}
                                    {{--  START --}}
                                    <div class="portfolio-massonary-four-items mix sosial all position-relative">
                                        <div class="modal fade" id="recentModalTen" tabindex="-1"
                                            aria-hidden="true">
                                            <div class="modal-dialog modal-dialog-centered justify-content-center">
                                                <div>
                                                    <button type="button" class="btn-close mb-10 btn"
                                                        data-bs-dismiss="modal" aria-label="Close"></button>
                                                    <img class="img-fluid" src="{{ url('images/sosial/3.jpeg') }}"
                                                        alt="portfolio img">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="img-overlay-full">
                                            <img class="img-fluid" src="{{ url('images/sosial/3.jpeg') }}"
                                                alt="portfolio img">
                                        </div>
                                        <div class="w-100 justify-content-center align-items-center pt-15">
                                            <p class="secondary-black fw-400 line-height-7 text-uppercase"><!-- Judul
                                                Kegiatan --></p>
                                            <a data-bs-toggle="modal" data-bs-target="#recentModalTen"
                                                href="#recentModalTen"
                                                class="h4 black-color fw-600 line-height-3 portfolio-massonary-item-link">
                                                <!-- Deskripsi Kegiatan -->
                                            </a>
                                        </div>
                                    </div>
                                    {{--  EnD --}}

                                    {{--  START --}}
                                    <div class="portfolio-massonary-four-items mix keluarga all position-relative">
                                        <div class="modal fade" id="recentModalTen" tabindex="-1"
                                            aria-hidden="true">
                                            <div class="modal-dialog modal-dialog-centered justify-content-center">
                                                <div>
                                                    <button type="button" class="btn-close mb-10 btn"
                                                        data-bs-dismiss="modal" aria-label="Close"></button>
                                                    <img class="img-fluid" src="{{ url('images/keluarga/1.jpeg') }}"
                                                        alt="portfolio img">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="img-overlay-full">
                                            <img class="img-fluid" src="{{ url('images/keluarga/1.jpeg') }}"
                                                alt="portfolio img">
                                        </div>
                                        <div class="w-100 justify-content-center align-items-center pt-15">
                                            <p class="secondary-black fw-400 line-height-7 text-uppercase"><!-- Judul
                                                Kegiatan --></p>
                                            <a data-bs-toggle="modal" data-bs-target="#recentModalTen"
                                                href="#recentModalTen"
                                                class="h4 black-color fw-600 line-height-3 portfolio-massonary-item-link">
                                                <!-- Deskripsi Kegiatan -->
                                            </a>
                                        </div>
                                    </div>
                                    {{--  EnD --}}

                                    {{--  START --}}
                                    <div class="portfolio-massonary-four-items mix keluarga all position-relative">
                                        <div class="modal fade" id="recentModalTen" tabindex="-1"
                                            aria-hidden="true">
                                            <div class="modal-dialog modal-dialog-centered justify-content-center">
                                                <div>
                                                    <button type="button" class="btn-close mb-10 btn"
                                                        data-bs-dismiss="modal" aria-label="Close"></button>
                                                    <img class="img-fluid" src="{{ url('images/keluarga/2.jpeg') }}"
                                                        alt="portfolio img">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="img-overlay-full">
                                            <img class="img-fluid" src="{{ url('images/keluarga/2.jpeg') }}"
                                                alt="portfolio img">
                                        </div>
                                        <div class="w-100 justify-content-center align-items-center pt-15">
                                            <p class="secondary-black fw-400 line-height-7 text-uppercase"><!-- Judul
                                                Kegiatan --></p>
                                            <a data-bs-toggle="modal" data-bs-target="#recentModalTen"
                                                href="#recentModalTen"
                                                class="h4 black-color fw-600 line-height-3 portfolio-massonary-item-link">
                                                <!-- Deskripsi Kegiatan -->
                                            </a>
                                        </div>
                                    </div>
                                    {{--  EnD --}}

                                    {{--  START --}}
                                    <div class="portfolio-massonary-four-items mix keluarga all position-relative">
                                        <div class="modal fade" id="recentModalTen" tabindex="-1"
                                            aria-hidden="true">
                                            <div class="modal-dialog modal-dialog-centered justify-content-center">
                                                <div>
                                                    <button type="button" class="btn-close mb-10 btn"
                                                        data-bs-dismiss="modal" aria-label="Close"></button>
                                                    <img class="img-fluid" src="{{ url('images/keluarga/3.jpeg') }}"
                                                        alt="portfolio img">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="img-overlay-full">
                                            <img class="img-fluid" src="{{ url('images/keluarga/3.jpeg') }}"
                                                alt="portfolio img">
                                        </div>
                                        <div class="w-100 justify-content-center align-items-center pt-15">
                                            <p class="secondary-black fw-400 line-height-7 text-uppercase"><!-- Judul
                                                Kegiatan --></p>
                                            <a data-bs-toggle="modal" data-bs-target="#recentModalTen"
                                                href="#recentModalTen"
                                                class="h4 black-color fw-600 line-height-3 portfolio-massonary-item-link">
                                                <!-- Deskripsi Kegiatan -->
                                            </a>
                                        </div>
                                    </div>
                                    {{--  EnD --}}

                                    {{--  START --}}
                                    <div class="portfolio-massonary-four-items mix keluarga all position-relative">
                                        <div class="modal fade" id="recentModalTen" tabindex="-1"
                                            aria-hidden="true">
                                            <div class="modal-dialog modal-dialog-centered justify-content-center">
                                                <div>
                                                    <button type="button" class="btn-close mb-10 btn"
                                                        data-bs-dismiss="modal" aria-label="Close"></button>
                                                    <img class="img-fluid" src="{{ url('images/keluarga/4.png') }}"
                                                        alt="portfolio img">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="img-overlay-full">
                                            <img class="img-fluid" src="{{ url('images/keluarga/4.png') }}"
                                                alt="portfolio img">
                                        </div>
                                        <div class="w-100 justify-content-center align-items-center pt-15">
                                            <p class="secondary-black fw-400 line-height-7 text-uppercase"><!-- Judul
                                                Kegiatan --></p>
                                            <a data-bs-toggle="modal" data-bs-target="#recentModalTen"
                                                href="#recentModalTen"
                                                class="h4 black-color fw-600 line-height-3 portfolio-massonary-item-link">
                                                <!-- Deskripsi Kegiatan -->
                                            </a>
                                        </div>
                                    </div>
                                    {{--  EnD --}}
                                    {{--  START --}}
                                    <div class="portfolio-massonary-four-items mix keluarga all position-relative">
                                        <div class="modal fade" id="recentModalTen" tabindex="-1"
                                            aria-hidden="true">
                                            <div class="modal-dialog modal-dialog-centered justify-content-center">
                                                <div>
                                                    <button type="button" class="btn-close mb-10 btn"
                                                        data-bs-dismiss="modal" aria-label="Close"></button>
                                                    <img class="img-fluid" src="{{ url('images/keluarga/5.jpeg') }}"
                                                        alt="portfolio img">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="img-overlay-full">
                                            <img class="img-fluid" src="{{ url('images/keluarga/5.jpeg') }}"
                                                alt="portfolio img">
                                        </div>
                                        <div class="w-100 justify-content-center align-items-center pt-15">
                                            <p class="secondary-black fw-400 line-height-7 text-uppercase"><!-- Judul
                                                Kegiatan --></p>
                                            <a data-bs-toggle="modal" data-bs-target="#recentModalTen"
                                                href="#recentModalTen"
                                                class="h4 black-color fw-600 line-height-3 portfolio-massonary-item-link">
                                                <!-- Deskripsi Kegiatan -->
                                            </a>
                                        </div>
                                    </div>
                                    {{--  EnD --}}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- end portfolio gallery -->

        <!-- start contact -->
        <section id="contact" class="home-four-contact">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <div class="home-contact-wrapper">
                            <div class="home-contact-info-container row align-items-center">
                                <div class="col-12 col-md-6 home-two-contact-info-col">
                                    <div class="section-heading">
                                        <div class="sub-heading d-flex align-items-center">
                                            <img src="{{ url('frontpage/img/orangeDot.png') }}" alt="orange-dot">
                                            <p>Kontak Kami</p>
                                        </div>
                                        <h2 class="black-color line-height-3 h2">
                                            Sampaikan Aspirasi Anda
                                        </h2>
                                    </div>
                                    <div class="row row-mobile-margin gy-3 gy-sm-0 row-mobile-margin mt-50">
                                        <div class="col-12 d-flex align-items-center">
                                            <div class="mr-10">
                                                <div class="light-orange-bg-icon">
                                                    <i class="fa-solid fa-mobile orange-color h4"></i>
                                                </div>
                                            </div>
                                            <div class="about-years-experience">
                                                <p class="fw-400 secondary-black p">Whatsapp</p>
                                                <h4 class="fw-500 black-color h4">
                                                    <a href="https://wa.me/6282115290404" target="_blank">+6282115290404</a>
                                                </h4>
                                            </div>
                                        </div>
                                        <div class="col-12 d-flex align-items-center row-mobile-margin mt-35">
                                            <div class="mr-10">
                                                <div class="light-orange-bg-icon">
                                                    <i class="fa-solid fa-envelope orange-color h4"></i>
                                                </div>
                                            </div>
                                            <div class="about-years-experience">
                                                <p class="fw-400 secondary-black p">Email</p>
                                                <h4 class="fw-500 black-color h4">
                                                    <a href="mailto:ppp@yuyunhidayat"
                                                        target="_blank">ppp@yuyunhidayat.id</a>
                                                </h4>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-12 col-md-6 row-mobile-margin">
                                <form id="FormSahate" name="FormSahate">
                                        <div class="row g-4">
                                            <div class="col-12 col-sm-6">
                                                <input type="email" name="email" class="form-control home-four-contact-input"
                                                    placeholder="Email (Opsional)">
                                            </div>
                                            <div class="col-12 col-sm-6">
                                                <input type="tel" name="nomor_handphone" class="form-control home-four-contact-input"
                                                    placeholder="Nomor Handphone (Opsional)">
                                            </div>

                                            <div class="col-12">
                                                <textarea name="aspirasi" class="form-control home-four-contact-input home-four-textarea"
                                                    placeholder="Aspirasi yang akan disampaikan"></textarea>
                                            </div>
                                            
                                            <div class="col-12">
                                            <label class="font-weight-bold text-uppercase">Captcha</label>
                                                <div class="col-md-6 captcha">
                                                <span>{!! captcha_img() !!}</span>
                                                <button type="button" class="btn btn-primary" class="reload" id="reload">
                                                    &#x21bb;
                                                </button>
                                                x</div>



                                            </div>

                                            <div class="col-12">
                                            <label class="font-weight-bold text-uppercase">Masukan Captcha</label>
                                                <input id="captcha" type="text" class="form-control" placeholder="Masukan Captcha" name="captcha">
    

                                            </div>
                        
                                            <div class="col-12">
                                                <div class="col-12">
                                                    <button type="submit" id="saveBtn" class="btn orange-btn btn_effect">
                                                        <span class="position-relative z-1">
                                                            Kirim
                                                        </span>
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- end contact -->
        <!-- start footer -->
        <section class="footer">
            <div class="footer-wrapper">
                <div class="footer-top">
                    <div class="container">
                        <div class="row">

                            <div class="col-12 col-sm-12 col-lg-12 mt-5 mt-lg-0">
                                <div class="w-100">
                                    <h2 class="text-white line-height-3 h2-secondary fw-700 text-start">
                                        Menuju Indonesia Sejahtera
                                    </h2>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="footer-bottom">
                    <div class="container">
                        <div class="footer-bottom-container">
                            <div class="row">
                                <div class="footerbottom-left col-12 col-sm-5 col-md-6 text-start">
                                    <p class="p text-white line-height-7 ">
                                        All rights reserved &copy; 2023 Relawan Bayu
                                    </p>
                                </div>
                                <div class="footerbottom-right col-12 col-sm-7 col-md-6">
                                    <p class="p text-white line-height-7 text-sm-end">
                                        Developed By <span class="orange-color">Relawan Bayu</span>
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- end footer -->
    </main>
    <!-- Preloader -->
    <!-- <div class="loader-mask">
        <div class="loader">
            <div></div>
            <div></div>
        </div>
    </div>
 -->
    <!-- Jquery script -->
    <script src="{{ url('frontpage/js/jquery-3.6.4.min.js') }}"></script>
    <!-- Bootstrap Script -->
    <script src="{{ url('frontpage/js/bootstrap.min.js') }}"></script>
    <!-- Magnific-popup script -->
    <script src="{{ url('frontpage/js/jquery.magnific-popup.min.js') }}"></script>
    <!-- Owl carousel script -->
    <script src="{{ url('frontpage/js/owl.carousel.min.js') }}"></script>
    <!-- MeanMenu script -->
    <script src="{{ url('frontpage/js/jquery.meanmenu.js') }}"></script>
    <!-- Line progressbar script -->
    <script src="{{ url('frontpage/js/jquery.lineProgressbar.js') }}"></script>
    <!-- Multi animated counter -->
    <script src="{{ url('frontpage/js/multi-animated-counter.js') }}"></script>
    <!-- One page nav script -->
    <script src="{{ url('frontpage/js/onepageNav.js') }}"></script>
    <!-- font awesome script -->
    <script src="{{ url('frontpage/js/all.min.js') }}"></script>
    <!-- AOS script -->
    <script src="{{ url('frontpage/js/aos.js') }}"></script>
    <!-- Animated headline script -->
    <script src="{{ url('frontpage/js/jquery.animatedheadline.min.js') }}"></script>
    <!-- mixitup -->
    <script src="{{ url('frontpage/js/mixitup.min.js') }}"></script>
    <!-- jQuery Counterup -->
    <script src="{{ url('frontpage/js/jquery.waypoints.min.js') }}"></script>
    <script src="{{ url('frontpage/js/jquery.counterup.min.js') }}"></script>
    <!-- Main JS script -->
    <script src="{{ url('frontpage/js/main.js') }}"></script>
    <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
<script type="text/javascript">
    $(function () {
        $.ajaxSetup({
          headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          }
    });

      $('#saveBtn').click(function (e) {
          e.preventDefault();
          $('#saveBtn').html('Mengirim Data ..');

          $.ajax({
            data: $('#FormSahate').serialize(),
            url: "simpan-aspirasi",
            type: "POST",
            dataType: 'json',
            success: function (data) {
                console.log('Error:', data);
                $('#FormSahate').trigger("reset");
                Swal.fire(
                    'Aspirasi Berhasil Disampaikan',
                    'Terima Kasih Anda Telah Menyampaikan Aspirasi',
                    'success'
                ).then(function (result) {
                if (result.value) {
                    window.location = "/";
                }
            })
                
                $('#saveBtn').html('Kirim');
        
            },
            error: function (data) {
                console.log('Error:', data);
                Swal.fire(
                    'Terdapat Kesalahan',
                    data.responseJSON.message,
                    'error'
                )
                $('#saveBtn').html('Kirim');
            }
        });
      });


    });
  </script>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  <script type="text/javascript">
  let input = document.getElementById('captcha');
    $('#reload').click(function () {
        $.ajax({
            type: 'GET',
            url: 'reload-captcha',
            success: function (data) {
                $(".captcha span").html(data.captcha);
                input.value = '';
                input.focus();
            }
        });
    });
</script>

</body>

</html>
