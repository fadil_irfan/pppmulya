@extends('layouts.app', ['title' => 'Login - Finance Operational UNSAP'])

@section('content')

<div class="col col-sm-6 col-md-6 col-lg-6 col-xl-6">
    <div class="card border-0 shadow rounded">
        <div class="card-body">
            @include('flash-message')
            <h4 class="font-weight-bold">LOGIN KOORDINATOR RELAWAN </h4>
            <hr>
            <form action="{{ route('cek-koordinator') }}" method="POST">
                @csrf
                <div class="form-group">
                    <label class="font-weight-bold text-uppercase">Nomor Handphone :</label>
                    <input type="text" name="nohp" value="{{ old('nohp') }}" class="form-control @error('nohp') is-invalid @enderror" placeholder="Masukkan Nomor Handphone">
                    @error('nohp')
                        <div class="alert alert-danger mt-2">
                            {{ $message }}
                        </div>
                    @enderror
                </div>
                
                <button type="submit" class="btn btn-primary"> <i class="fas fa-sign-in-alt"></i> MASUK</button>
                <hr>
            </form>
        </div>
    </div>

</div>

@endsection
