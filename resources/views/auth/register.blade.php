@extends('layouts.app', ['title' => 'Daftar - Sahabat Haji Yuyun Hidayat'])
@section('content')

<div class="col-md-8">
    <div class="card border-0 shadow rounded">
        <div class="card-body">
            <h4 class="font-weight-bold">Daftar Relawan </h4>
            <hr>
            <form id="FormSahate" name="FormSahate">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="font-weight-bold text-uppercase">Nomor Induk Kependudukan (NIK)</label>
                            <input type="number" name="nik" value="{{ old('nik') }}" class="form-control @error('nik') is-invalid @enderror" placeholder="Masukkan Nomor Induk Kependudukan (NIK)">
                            @error('nik')
                                <div class="alert alert-danger mt-2">
                                    {{ $message }}
                                </div>
                            @enderror
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="font-weight-bold text-uppercase">Nama Lengkap</label>
                            <input type="text" name="nama_lengkap" value="{{ old('nama_lengkap') }}" class="form-control @error('nama_lengkap') is-invalid @enderror" placeholder="Masukkan Nama Lengkap">
                            @error('nama_lengkap')
                                <div class="alert alert-danger mt-2">
                                    {{ $message }}
                                </div>
                            @enderror
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="font-weight-bold text-uppercase">Nomor Whatsapp (Format : 08xxxxxxxx)</label>
                            <input type="text" name="nomor_wa" value="{{ old('nomor_wa') }}" class="form-control @error('nomor_wa') is-invalid @enderror" placeholder="Masukkan Nomor Whatsapp" data-mdb-input-mask="+62 999-999-999">
                            @error('nomor_wa')
                                <div class="alert alert-danger mt-2">
                                    {{ $message }}
                                </div>
                            @enderror
                        </div>
                    </div>

                    <div class="col-sm-6">
                        <div class="form-group">
                            <label for="name" class="font-weight-bold text-uppercase">Kabupaten </label>
                                <select class="form-control" id="kabupaten" name="kabupaten" placeholder="Pilih Kabupaten" required>
                                <option value="" >Pilih Kabupaten </option>
                                @foreach ($kabupaten as $kabupaten)
                                    <option value="{{ $kabupaten->kode_kab }}" >
                                        {{ $kabupaten->nama_kab }}
                                    @endforeach

                                </select>
                           
                        </div>

                    </div>

                    

                    

                </div>

               

                <div class="row">
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label for="name" class="font-weight-bold text-uppercase">Kecamatan </label>
                                <select class="form-control" id="kecamatan" name="kecamatan" placeholder="Pilih Kecamatan" required>
                                <option value="" >Pilih Kecamatan </option>
                                
                                </select>
                           
                        </div>

                    </div>

                    <div class="col-sm-6">
                        <div class="form-group">
                            <label for="name" class="font-weight-bold text-uppercase">Desa/Kelurahan </label>
                            <select class="form-control" id="kelurahan" name="kelurahan" placeholder="Pilih Desa/Kelurahan" required>
                                <option value="" >Pilih Desa/Kelurahan </option>
                                </select>
                           
                        </div>

                    </div>

                </div>

                <div class="row">

                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="font-weight-bold text-uppercase">RT</label>
                            <input type="number" name="rt" value="{{ old('rt') }}" class="form-control @error('rt') is-invalid @enderror" placeholder="RT">
                            @error('rt')
                                <div class="alert alert-danger mt-2">
                                    {{ $message }}
                                </div>
                            @enderror
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="font-weight-bold text-uppercase">RW</label>
                            <input type="number" name="rw" value="{{ old('rw') }}" class="form-control @error('rw') is-invalid @enderror" placeholder="RW">
                            @error('rw')
                                <div class="alert alert-danger mt-2">
                                    {{ $message }}
                                </div>
                            @enderror
                        </div>
                    </div>

                    

                    

                </div>

                <button type="submit" id="saveBtn" class="btn btn-primary"><i class="fas fa-save"></i> DAFTAR</button>
                <a href={{url('/')}} class="btn btn-info"> <i class="fas fa-sign-out-alt"></i> KEMBALI</a>
                <p>Sudah Daftar Sebelumnya ? Silahkan Login di tombol dibawah ini :</p>
                <a href={{url('/login-koordinator')}} class="btn btn-success"> <i class="fas fa-sign-in-alt"></i> Login</a>

                
                
                
            </form>
        </div>
    </div>
   
</div>

@endsection
@push('page-script')

<script>
    $(document).ready(function() {
        $('#kabupaten').on('change', function() {
            let data = {
                kabupaten: $(this).val(),
            }

            $.ajax({
                type: 'get',
                url: 'ref-wilayah-kabupaten',
                data: data,
                dataType: 'json',
                beforeSend: function () {
                    $('#kecamatan').html(' <option value="" >Pilih Kecamatan </option>');
                    
                },
                success: function(response) {
                    $.each(response.data, function(i, val) {
                        $('#kecamatan').append(
                            `
                            <option value="${val.kode_kec}">${val.nama_kec}</option>
                            `
                        );
                    });
                   
                }
            });
        });

        $('#kecamatan').on('change', function() {
            let data = {
                kecamatan: $(this).val(),
            }

            $.ajax({
                type: 'get',
                url: 'ref-wilayah-kecamatan',
                data: data,
                dataType: 'json',
                beforeSend: function () {
                    $('#kelurahan').html('');
                },
                success: function(response) {
                    $.each(response.data, function(i, val) {
                        $('#kelurahan').append(
                            `
                            <option value="${val.kode_kel}">${val.nama_kel}</option>
                            `
                        );
                    });
                   
                }
            });
        });

    });
</script>
<script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
<script type="text/javascript">
    $(function () {
        $.ajaxSetup({
          headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          }
    });

      $('#saveBtn').click(function (e) {
          e.preventDefault();
          $('#saveBtn').html('Mengirim Data ..');

          $.ajax({
            data: $('#FormSahate').serialize(),
            url: "simpan-masyarakat",
            type: "POST",
            dataType: 'json',
            success: function (data) {
                //console.log('Error:', data);
                $('#FormSahate').trigger("reset");
                Swal.fire(
                    'Pendaftaran Berhasil',
                    'Terima Kasih Anda Telah Menjadi Relawan H. Yuyun Hidayat',
                    'success'
                ).then(function (result) {
                if (result.value) {
                    window.location = "register";
                }
            })
                
                $('#saveBtn').html('DAFTAR');
        
            },
            error: function (data) {
                console.log('Error:', data);
                Swal.fire(
                    'Terdapat Kesalahan',
                    data.responseJSON.message,
                    'error'
                )
                $('#saveBtn').html('DAFTAR');
            }
        });
      });


    });
  </script>
@endpush
