@extends('layouts.master-template')
@push('page-script')
<style type="text/css">
.dataTables_filter input {
    width: 450px !important;
}
table.dataTable thead tr {
  background-color: #28a745;
  color:white
}
</style>

@endpush

@section('contents')
<div class="container">
    <h1>Kelola Data Masyarakat </h1>
   {{--  <a class="btn btn-success" href="javascript:void(0)" id="createNewCustomer"><i class="fas fa-save"></i> Tambah Data</a>
    <br/> --}}
    <br/>
    <table class="table table-bordered data-table">
        <thead>
            <tr>
                <th>No</th>
                <th width="10%">NIK</th>
                <th width="20%" class="text-center">NAMA LENGKAP</th>
                <th width="20%" class="text-center">ALAMAT</th>
                <th width="20%" class="text-center">Whatsapp</th>
                <th width="20%" class="text-center">KOORDINATOR</th>
                <th width="20%">Action</th>
            </tr>
        </thead>
        <tbody>
        </tbody>
    </table>
</div>

<div class="modal fade" id="ajaxModel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="modelHeading"></h4>
            </div>
            <div class="modal-body">
                <form id="CustomerForm" name="CustomerForm" class="form-horizontal">
                   <input type="hidden" name="idPengeluaran" id="idPengeluaran">
                    <div class="form-group">
                        <label for="name" class="col-sm-2 control-label">Kode</label>
                        <div class="col-sm-12">
                            <input type="text" class="form-control" id="kode" name="kode" placeholder="Masukan Kode" value="" maxlength="50" required="" autofocus>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="name" class="col-sm-2 control-label">Istilah</label>
                        <div class="col-sm-12">
                            <input type="text" class="form-control" id="istilah" name="nama_pengeluaran" placeholder="Masukan Istilah" value="" required="">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label">Penjelasan</label>
                        <div class="col-sm-12">
                            <textarea id="penjelasan" name="penjelasan" required="" placeholder="Masukan Penjelasan" class="form-control"></textarea>
                        </div>
                    </div>

                    <div class="col-sm-offset-2 col-sm-10">
                     <button type="submit" class="btn btn-primary" id="saveBtn" value="create"><i class="fas fa-save"></i> Simpan
                     </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>


@endsection


@push('page-script')
<script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>

<script type="text/javascript">
    $(function () {
        $.ajaxSetup({
          headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          }
    });

      var table = $('.data-table').DataTable({
          processing: true,
          serverSide: true,
          ajax: "",
          columns: [
              {data: 'DT_RowIndex', name: 'DT_RowIndex'},
              {data: 'nik', name: 'nik'},
              {data: 'nama_lengkap', name: 'nama_lengkap'},
              {data: 'alamat', name: 'alamat'},
              {data: 'nomor_wa', name: 'nomor_wa'},
              {data: 'koordinator', name: 'koordinator'},
              {data: 'action', name: 'action', orderable: false, searchable: false},
          ],
          'columnDefs': [
            {
                "targets": 0, // your case first column
                "className": "text-center",
            },
            {
                "targets": 1, // your case first column
                "className": "text-center",
            },
            {
                "targets": 2,
                "className": "text-left",
            },
            {
                "targets": 4, // your case first column
                "className": "text-center",
            },
 ]
      });

      $('#ajaxModel').on('shown.bs.modal', function() {
        $('input[name="kode"]').focus();
    });

      $('#createNewCustomer').click(function () {
          $('#saveBtn').val("create-Customer");
          $('#Customer_id').val('');
          $('#CustomerForm').trigger("reset");
          $('#modelHeading').html("Tambah Data Master Pengeluaran");
          $('#ajaxModel').modal('show');
      });

      $('body').on('click', '.editCustomer', function () {
        var Customer_id = $(this).data('id');
        $.get("master-pengeluaran" +'/' + Customer_id +'/edit', function (data) {
            $('#modelHeading').html("Edit Data Master Pengeluaran");
            $('#saveBtn').val("edit-user");
            $('#ajaxModel').modal('show');
            $('#idPengeluaran').val(data.id_pengeluaran);
            $('#kode').val(data.kode);
            $('#istilah').val(data.nama_pengeluaran);
            $('#penjelasan').val(data.penjelasan);
        })
     });

      $('#saveBtn').click(function (e) {
          e.preventDefault();
          $(this).html('Sending..');

          $.ajax({
            data: $('#CustomerForm').serialize(),
            url: "",
            type: "POST",
            dataType: 'json',
            success: function (data) {

                $('#CustomerForm').trigger("reset");
                $('#ajaxModel').modal('hide');
                table.draw();
                if (data.status=='200')
                {
                    Swal.fire(
                    'Good job!',
                    data.success,
                    'success'
                );
                }else
                {
                    Swal.fire(
                    'Error !',
                    data.success,
                    'error'
                );
                }

                $('#saveBtn').html('<i class="fas fa-save"></i> Simpan');


            },
            error: function (data) {
                console.log('Error:', data);
                $('#saveBtn').html('<i class="fas fa-save"></i> Simpan');
            }
        });
      });

      $('body').on('click', '.deleteCustomer', function () {

          var Customer_id = $(this).data("id");
          Swal.fire({
                title: 'Do you want to save the changes?',
                showCancelButton: true,
                confirmButtonText: '<i class="fas fa-save"></i> Simpan',
        }).then((result) => {
            /* Read more about isConfirmed, isDenied below */
            if (result.isConfirmed) {
                $.ajax({
                    type: "DELETE",
                    url: "master-pengeluaran/delete"+'/'+Customer_id,
                    success: function (data) {
                        table.draw();
                        Swal.fire(data.success, '', 'success')
                    },
                    error: function (data) {
                        console.log('Error:', data);
                    }
                });

            } else if (result.isDenied) {
                Swal.fire('Tidak Terjadi Perubahan Data', '', 'info')
            }
        })


      });

    });
  </script>

@endpush
