@extends('layouts.master-template')
@push('page-script')

<link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.css" />

@endpush

@section('contents')
<div class="container">

    <div class="card">
        <div class="card-header">
            Form Jurnal Rekap Penerimaan/Pengeluaran
        </div>
        <div class="card-body">
            <form id="CustomerForm" name="CustomerForm" action="{{ url('report-rekap') }}" class="form-horizontal" method="POST">
                @csrf
                <input type="hidden" name="id" id="id">
                <div class="row">
                 <div class="col-sm-12">
                     <div class="form-group">
                         <label for="name" class="col-sm-6 control-label">Periode Tanggal</label>
                         <div class="col-sm-6">
                            <input type="text" class="form-control"  name="daterange" placeholder="Periode 1" value="" required="" >
                        </div>


                     </div>

                 </div>

                </div>

                 <div class="row">
                     <div class="col-sm-9">
                        <div class="col-sm-offset-2 col-sm-12">
                            <button type="submit" class="btn btn-primary" id="saveBtn" value="create" formtarget="_blank"><i class="fas fa-sync"></i> Proses
                            </button>
                           </div>
                     </div>
                     <div class="col-sm-2">


                     </div>
                 </div>


             </form>
        </div>
    </div>


</div>

@endsection


@push('page-script')
<script type="text/javascript" src="//cdn.jsdelivr.net/jquery/1/jquery.min.js"></script>
<script type="text/javascript" src="//cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<!-- Include Date Range Picker -->
<script type="text/javascript" src="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.js"></script>

<script type="text/javascript">
    $(function() {
        $('input[name="daterange"]').daterangepicker();
    });
    </script>



@endpush
