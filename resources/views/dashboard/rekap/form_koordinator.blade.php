@extends('layouts.master-template')
@push('page-script')

<link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.css" />

@endpush

@section('contents')
<div class="container">

    <div class="card">
        <div class="card-header">
            Form Pencarian Rekap Koordinator
        </div>
        <div class="card-body">
            <form id="CustomerForm" name="CustomerForm" action="{{ url('report-harian') }}" class="form-horizontal" method="POST">
                @csrf
                <input type="hidden" name="id" id="id">
                <div class="row">
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label for="name" class="font-weight-bold text-uppercase">Kabupaten </label>
                                <select class="form-control" id="kabupaten" name="kabupaten" placeholder="Pilih Kabupaten" required>
                                <option value="" >Pilih Kabupaten </option>
                                @foreach ($kabupaten as $kabupaten)
                                    <option value="{{ $kabupaten->kode_kab }}" >
                                        {{ $kabupaten->nama_kab }}
                                    @endforeach

                                </select>
                           
                        </div>

                    </div>

                    <div class="col-sm-6">
                        <div class="form-group">
                            <label for="name" class="font-weight-bold text-uppercase">Kecamatan </label>
                                <select class="form-control" id="kecamatan" name="kecamatan" placeholder="Pilih Kecamatan" required>
                                <option value="" >Pilih Kecamatan </option>
                                
                                </select>
                           
                        </div>

                    </div>

                    <div class="col-sm-6">
                        <div class="form-group">
                            <label for="name" class="font-weight-bold text-uppercase">Desa/Kelurahan </label>
                            <select class="form-control" id="kelurahan" name="kelurahan" placeholder="Pilih Desa/Kelurahan" required>
                                <option value="" >Pilih Desa/Kelurahan </option>
                                </select>
                           
                        </div>

                    </div>

                </div>

                 <div class="row">
                     <div class="col-sm-9">
                        <div class="col-sm-offset-2 col-sm-12">
                            <button type="submit" class="btn btn-primary" id="saveBtn" value="create" formtarget="_blank"><i class="fas fa-search"></i> Cari
                            </button>
                           </div>
                     </div>
                     <div class="col-sm-2">


                     </div>
                 </div>


             </form>
        </div>
    </div>


</div>

@endsection


@push('page-script')
<script type="text/javascript" src="//cdn.jsdelivr.net/jquery/1/jquery.min.js"></script>
<script type="text/javascript" src="//cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<!-- Include Date Range Picker -->
<script type="text/javascript" src="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.js"></script>

<script type="text/javascript">
    $(function() {
        $('input[name="daterange"]').daterangepicker();
    });
    </script>
<script>
    $(document).ready(function() {
        $('#kabupaten').on('change', function() {
            let data = {
                kabupaten: $(this).val(),
            }

            $.ajax({
                type: 'get',
                url: 'ref-wilayah-kabupaten',
                data: data,
                dataType: 'json',
                beforeSend: function () {
                    $('#kecamatan').html(' <option value="" >Pilih Kecamatan </option>');
                    
                },
                success: function(response) {
                    $.each(response.data, function(i, val) {
                        $('#kecamatan').append(
                            `
                            <option value="${val.kode_kec}">${val.nama_kec}</option>
                            `
                        );
                    });
                   
                }
            });
        });

        $('#kecamatan').on('change', function() {
            let data = {
                kecamatan: $(this).val(),
            }

            $.ajax({
                type: 'get',
                url: 'ref-wilayah-kecamatan',
                data: data,
                dataType: 'json',
                beforeSend: function () {
                    $('#kelurahan').html('');
                },
                success: function(response) {
                    $.each(response.data, function(i, val) {
                        $('#kelurahan').append(
                            `
                            <option value="${val.kode_kel}">${val.nama_kel}</option>
                            `
                        );
                    });
                   
                }
            });
        });

    });
</script>


@endpush


