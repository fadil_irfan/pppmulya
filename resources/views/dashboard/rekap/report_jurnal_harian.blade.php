<html>
<head>
<style>
    .alnright { text-align: right; }

    .alncenter { text-align: center; }


    div.breakNow {
        page-break-inside:avoid; page-break-after:always;
    }
    .pagenum:before {
        content: counter(page);
        text-align:center;
    }
.header{
    width:1440px;
    font-weight:bold;
    text-align:center;
}
.table {
    width:100%;
    border-collapse: collapse;
    font-size: 12px;
    margin-bottom: 10px;
}
.table th {
    border: 1px solid #000;
}
.table td {
    border: 1px solid #000;
    padding:5px 5px;
}
h2{
    text-align:center;
}

#customers {
  font-family: Arial, Helvetica, sans-serif;
  border-collapse: collapse;
  width: 100%;
}

#customers td, #customers th {
  border: 1px solid #ddd;
}

#customers tr:nth-child(even){background-color: #f2f2f2;}

#customers tr:hover {background-color: #ddd;}

#customers th {
  text-align: center;
  background-color: #04AA6D;
  color: white;
}

#ttd {
  font-family: Arial, Helvetica, sans-serif;
  border:none;
  width: 100%;
}

#ttd td, #ttd th {
    border:none;
}



#ttd th {
  padding-top: 12px;
  padding-bottom: 12px;
  text-align: center;

}
</style>
</head>
<body>
<h2 >{{ $unit_fakultas->nama_unit}} </h2>
<p style="text-align: center">{{ $unit_fakultas->alamat_unit}}</p>

<h2 >JURNAL HARIAN</h2>
<p style="text-align: center">{{ $tanggal_1.' s.d '.$tanggal_2 }}</p>
<table id="customers">
    <tr>
      <th>Tanggal</th>
      <th>Uraian</th>
      <th>Nomor</th>
      <th>Penerimaan</th>
      <th>Pengeluaran</th>
      <th>Saldo</th>
    </tr>
    {{ $saldo=0 }}
    {{ $penerimaan_total=0 }}
    {{ $pengeluaran_total=0 }}
    {{ $i=0 }}
    @forelse($datapilih as $item)
    <tr>
        <td class="alncenter" >{{ $item->waktu }}</td>
        <td >{{ $item->deskripsi_penerimaan }}</td>
        <td class="alncenter" >{{ $item->no_transaksi_masuk }}</td>
        <td class="alnright" >{{ number_format($item->jumlah_penerimaan,0) }}</td>
        <td class="alnright" >{{ number_format($item->jumlah_pengeluaran,0) }}</td>
        <td class="alnright" >{{ number_format($saldo+=$item->jumlah_penerimaan-$item->jumlah_pengeluaran,0)  }}</td>
        {{ $penerimaan_total+=$item->jumlah_penerimaan}}
        {{ $pengeluaran_total+=$item->jumlah_pengeluaran }}
    </tr>
    {{$i++}}
    @if( $i % 4 == 0 )
    <div class="breakNow"></div>
    @endif


    @empty
    <tr>
        <td colspan="6" class="alncenter">
            Data Tidak Ditemukan
        </td>
    </tr>
    @endforelse

    <tr>
        <th colspan="3">Total</th>
        <th class="alnright">{{ number_format($penerimaan_total,0)}}</th>
        <th class="alnright">{{number_format($pengeluaran_total,0)}}</th>
        <th class="alnright">{{ number_format($saldo,0) }}</th>
      </tr>


  </table>

<table style="font-size:14px;width:100%">
    <tbody>
        <tr>
            <td colspan="3" style="text-align:right"></td>
        </tr>
        <tr>
            <td colspan="3" style="text-align:right"></td>
        </tr>
        <tr>
            <td colspan="3" style="text-align:right">Sumedang, {{ $tanggal }}</td>
        </tr>
    </tbody>
    </table>

    <table id=ttd>
        <tbody>
            <tr >
                <th width="30%"  style="text-align:center;">{{ $unit_fakultas->jabatan_tanda_wadek}}</th>
                <th width="40%" style="text-align:center"></th>
                <th width="30%" style="text-align:center;">{{ $unit_fakultas->jabatan_tanda_kepala}}</th>
            </tr>
            <tr>
                <th width="30%"  style="text-align:center;"></th>
                <th width="40%" style="text-align:center"></th>
                <th width="30%" style="text-align:center;"></th>
            </tr>

            <tr>
                <th width="30%" style="text-align:center;">{{ $unit_fakultas->nama_tanda_wadek}}</th>
                <th width="40%" style="text-align:right"></th>
                <th width="30%" style="text-align:center;" >{{ $unit_fakultas->nama_tanda_kepala}}</th>
            </tr>

        </tbody>
        </table>

</body>
</html>
