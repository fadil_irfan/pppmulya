@extends('layouts.master-koordinator')
@section('contents')
    <div class="card">
        <div class="card-header">
          RELAWAN HAJI YUYUN HIDAYAT
        </div>
        <div class="card-body">
          <h5 class="card-title">Selamat Datang </h5>
          <strong>Nama Koordinator :</strong> <p class="card-text"><strong>{{ session('nama_lengkap') }}</strong></p>
          <strong>Kabupaten :</strong> <p class="card-text"><strong>{{ \App\Helpers\Helper::getKabupaten(session('kabupaten')) }}</strong></p>
          <strong>Kecamatan :</strong> <p class="card-text"><strong>{{ \App\Helpers\Helper::getKecamatan(session('kecamatan')) }}</strong></p>
          <strong>Kelurahan :</strong> <p class="card-text"><strong>{{ \App\Helpers\Helper::getKelurahan(session('kelurahan')) }}</strong></p>
        </div>
    </div>





<div class="line"></div>

<h2>Informasi Relawan</h2>

<div class="row">
    <div class="col-lg-4">
        <div class="card text-white bg-success mb-3" style="max-width: 18rem;">
            <div class="card-body text-center">
                <i class="fas fa-th-list"></i>
                <h3><span>{{ $total }}</span></h3>
                <p class="text-white font-15 mb-0">Jumlah Relawan</p>

            </div>
          </div>
    </div>
    <div class="col-lg-4">
        <div class="card text-white bg-primary mb-3" style="max-width: 18rem;">

            <div class="card-body text-center">
                <i class="fas fa-th-large"></i>
                <h3><span>{{ $relawan }}</span></h3>
                <p class="text-white font-15 mb-0">Jumlah Koordinator</p>

            </div>
          </div>
    </div>

    <div class="col-lg-4">
        <div class="card text-white bg-info mb-3" style="max-width: 18rem;">

            <div class="card-body text-center">
                <i class="fas fa-th"></i>
                <h3><span>{{ $masyarakat }}</span></h3>
                <p class="text-white font-15 mb-0">Jumlah Masyarakat</p>

            </div>
          </div>
    </div>
</div>


<div class="line"></div>

<h2>Sebaran Wilayah</h2>

<div class="row">
    <div class="col-lg-12">
        <div class="card text-white bg-success mb-3">
            <div class="card-body text-center">
                <table class="table table-bordered data-table">
                    <thead>
                        <tr>
                            <th width="10%">No</th>
                            <th width="30%">Nama Wilayah </th>
                            <th width="30%" class="text-center">Jumlah Masyarakat</th>
                        </tr>
                    </thead>
                    <tbody>
                       @php
                           $jumlah=0;
                       @endphp
                        @foreach ($wilayah as $wilayah)
                            <tr>
                                <td class="text-center">{{ $loop->iteration }}</td>
                                <td class="text-left">{{ \App\Helpers\Helper::getKabupaten($wilayah->kabupaten) }}, KEC. {{ \App\Helpers\Helper::getKecamatan($wilayah->kecamatan) }}, KEL. {{ \App\Helpers\Helper::getKelurahan($wilayah->kelurahan) }} </td>
                                <td>{{ \App\Helpers\Helper::getHitungWilayah($wilayah->kabupaten,$wilayah->kecamatan,$wilayah->kelurahan) }}</td>
                                @php
                                    $jumlah+=\App\Helpers\Helper::getHitungWilayah($wilayah->kabupaten,$wilayah->kecamatan,$wilayah->kelurahan);
                                @endphp
                            </tr>
                        @endforeach
                        <tr>
                            <td colspan="2">Total</td>
                            <td>{{ $jumlah }}</td>
                        </tr>
                    </tbody>
                </table>

            </div>
          </div>
    </div>
    
</div>


@endsection
