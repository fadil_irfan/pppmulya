@extends('layouts.master-template')
@section('contents')
    <div class="card">
        <div class="card-header">
          SAHABAT HAJI YUYUN HIDAYAT
        </div>
        <div class="card-body">
          <h5 class="card-title">Selamat Datang </h5>
          {{-- <strong>Pengguna :</strong> <p class="card-text"><strong>{{ auth()->user()->name }}</strong></p>
          <strong>Nama Unit :</strong> <p class="card-text"><strong>{{ $unit->nama_unit }}</strong></p> --}}
        </div>
    </div>





<div class="line"></div>

<h2>Informasi Relawan</h2>

<div class="row">
    <div class="col-lg-4">
        <div class="card text-white bg-success mb-3" style="max-width: 18rem;">
            <div class="card-body text-center">
                <i class="fas fa-th-list"></i>
                <h3><span>{{ $total }}</span></h3>
                <p class="text-white font-15 mb-0">Jumlah Relawan</p>

            </div>
          </div>
    </div>
    <div class="col-lg-4">
        <div class="card text-white bg-primary mb-3" style="max-width: 18rem;">

            <div class="card-body text-center">
                <i class="fas fa-th-large"></i>
                <h3><span>{{ $relawan }}</span></h3>
                <p class="text-white font-15 mb-0">Jumlah Koordinator</p>

            </div>
          </div>
    </div>

    <div class="col-lg-4">
        <div class="card text-white bg-info mb-3" style="max-width: 18rem;">

            <div class="card-body text-center">
                <i class="fas fa-th"></i>
                <h3><span>{{ $masyarakat }}</span></h3>
                <p class="text-white font-15 mb-0">Jumlah Masyarakat</p>

            </div>
          </div>
    </div>
</div>


<div class="line"></div>

<h2>Sebaran Wilayah</h2>

<div class="row">
    <div class="col-lg-12">
        <div class="card text-white bg-success mb-3">
            <div class="card-body text-center">
                <table class="table table-bordered data-table">
                    <thead>
                        <tr>
                            <th width="10%">No</th>
                            <th width="30%">Nama Wilayah </th>
                            <th width="30%" class="text-center">Jumlah Masyarakat</th>
                        </tr>
                    </thead>
                    <tbody>
                       @php
                           $jumlah=0;
                       @endphp
                        @foreach ($wilayah as $wilayah)
                            <tr>
                                <td class="text-center">{{ $loop->iteration }}</td>
                                <td class="text-left">{{ \App\Helpers\Helper::getKabupaten($wilayah->kabupaten) }}, KEC. {{ \App\Helpers\Helper::getKecamatan($wilayah->kecamatan) }}, KEL. {{ \App\Helpers\Helper::getKelurahan($wilayah->kelurahan) }} </td>
                                <td>{{ \App\Helpers\Helper::getHitungWilayah($wilayah->kabupaten,$wilayah->kecamatan,$wilayah->kelurahan) }}</td>
                                @php
                                    $jumlah+=\App\Helpers\Helper::getHitungWilayah($wilayah->kabupaten,$wilayah->kecamatan,$wilayah->kelurahan);
                                @endphp
                            </tr>
                        @endforeach
                        <tr>
                            <td colspan="2">Total</td>
                            <td>{{ $jumlah }}</td>
                        </tr>
                    </tbody>
                </table>

            </div>
          </div>
    </div>
    
</div>

<div class="line"></div>

<h2>Peta Sebaran</h2>
<div id="map">
</div>

@endsection
@section('styles')
<!-- Leaflet CSS -->
<link rel='stylesheet' href='https://unpkg.com/leaflet@1.8.0/dist/leaflet.css' crossorigin='' />
    <style>
      #map { min-height: 800px; }
    </style>
@endsection


@push('page-script')
<!-- Leaflet JavaScript -->
      <!-- Make sure you put this AFTER Leaflet's CSS -->
      
      <script src="https://unpkg.com/leaflet@1.7.1/dist/leaflet.js"
          integrity="sha512-XQoYMqMTK8LvdxXYG3nZ448hOEQiglfqkJs1NOQV44cWnUrBc8PkAOcXy20w0vlaXaVUearIOBhiXZ5V3ynxwA=="
          crossorigin="">
      </script>
      <script src="{{ asset('js/Subang.js') }}"></script>
      <script src="{{ asset('js/Sumedang.js') }}"></script>
      <script src="{{ asset('js/Majalengka.js') }}"></script>
      <script>
        let map, markers = [];
        /* ----------------------------- Initialize Map ----------------------------- */
        function initMap() {
           /*  map = L.map('map', {
                center: {
                    lat: -6.571589,
                    lng: 107.758736,
                },
                zoom: 15
            }); */
            map = L.map('map', {
                    scrollWheelZoom: false
            }).setView([-6.571589, 107.758736], 10);


            L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
                attribution: '© OpenStreetMap'
            }).addTo(map);
        }

        initMap();
        function style(feature) {
    return {
        fillColor: '#1CA43C',
        weight: 0.8,
        opacity: 0.8,
        color: 'white',
        dashArray: '0',
        fillOpacity: 0.4
    };
}

function onEachFeature(feature, layer) {
    layer.on({
        mouseover: highlightFeature,
        mouseout: resetHighlight,
        'click': function (e) {
              select(e.target);
              map.fitBounds(e.target.getBounds());
            }
    });
     
}


function zoomToFeature(e) {
    map.fitBounds(e.target.getBounds());
}

function highlightFeature(e) {
    var layer = e.target;

    layer.setStyle({
        weight: 5,
        color: 'white',
        fillColor:'red',
        dashArray: '',
        fillOpacity: 0.4
    });

    if (!L.Browser.ie && !L.Browser.opera && !L.Browser.edge) {
        layer.bringToFront();
    }

}




        var peta_subang = L.geoJson(Subang, {
                    style: style,
                    onEachFeature: onEachFeature
            })
         peta_subang.addTo(map); 

         var peta_sumedang = L.geoJson(Sumedang, {
                    style: style,
                    onEachFeature: onEachFeature
            })
         peta_sumedang.addTo(map); 

         var peta_majalengka = L.geoJson(Majalengka, {
                    style: style,
                    onEachFeature: onEachFeature
            })
         peta_majalengka.addTo(map); 

         function resetHighlight(e) {
            peta_subang.resetStyle(e.target);
        }

        function select(e) {
            //Zoom pada kecamatan yang diakses
        console.log(e.feature.geometry);	
		
		coords = []; 		
		
		var simpan_koordinat_kecamatan = e.feature.geometry.coordinates[0][0][0];
        var simpan_longitude_kecamatan = e.feature.geometry.coordinates[0][0][1];
		
		console.log(simpan_koordinat_kecamatan);		
        console.log(simpan_longitude_kecamatan);	
	}



    </script>
@endpush
