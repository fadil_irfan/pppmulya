@extends('layouts.master-template')
@push('page-script')
<style type="text/css">
.dataTables_filter input {
    width: 450px !important;
}
table.dataTable thead tr {
  background-color: #28a745;
  color:white
}
</style>

@endpush

@section('contents')
<div class="container">
    <h1>Master Pengguna </h1>
    <a class="btn btn-success" href="javascript:void(0)" id="createNewCustomer"><i class="fas fa-save"></i> Tambah Data</a>
    <br/>
    <br/>
    <table class="table table-bordered data-table">
        <thead>
            <tr>
                <th width="10%">No</th>
                <th width="30%">Nama </th>
                <th width="30%" class="text-center">Email</th>
                <th width="10%" class="text-center">Role</th>
                <th width="20%">Action</th>
            </tr>
        </thead>
        <tbody>
        </tbody>
    </table>
</div>

<div class="modal fade" id="ajaxModel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="modelHeading"></h4>
            </div>
            <div class="modal-body">
                <form id="CustomerForm" name="CustomerForm" class="form-horizontal">
                   <input type="hidden" name="id" id="id">
                   <div class="row">
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label for="name" class="col-sm-6 control-label">Nama Pengguna</label>
                            <div class="col-sm-12">
                                <input type="text" class="form-control" id="name" name="name" placeholder="Masukan Nama Pengguna" value=""  required autofocus>
                            </div>
                        </div>

                    </div>
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label for="name" class="col-sm-6 control-label">Email Pengguna</label>
                            <div class="col-sm-12">
                                <input type="text" class="form-control" id="email" name="email" placeholder="Masukan Email" value="" required="" >
                            </div>
                        </div>

                    </div>
                   </div>

                   <div class="row">
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label for="name" class="col-sm-6 control-label">Password</label>
                            <div class="col-sm-12">
                                <input type="password" class="form-control" id="password" name="password" placeholder="Masukan Password" value=""  required autofocus>
                            </div>
                        </div>

                    </div>
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label for="name" class="col-sm-12 control-label">Konfirmasi Password</label>
                            <div class="col-sm-12">
                                <input type="password" class="form-control" id="konfirmasi_password" name="konfirmasi_password" placeholder="Masukan Konfirmasi Password" value="" required="" >
                            </div>
                        </div>

                    </div>
                   </div>

                    <div class="row">

                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="name" class="col-sm-12 control-label">Role </label>
                                <div class="col-sm-12">
                                    <select class="form-control" id="role" name="role" placeholder="Pilih Role" required>
                                        <option value="admin">Admin</option>
                                        <option value="pengguna">Pengguna</option>
                                    </select>
                                </div>
                            </div>

                        </div>

                    </div>

                    <div class="row">
                        <div class="col-sm-9">

                        </div>
                        <div class="col-sm-3">
                            <div class="col-sm-offset-2 col-sm-12 ml-4">
                                <button type="submit" class="btn btn-primary" id="saveBtn" value="create"><i class="fas fa-save"></i> Simpan
                                </button>
                               </div>

                        </div>
                    </div>


                </form>
            </div>
        </div>
    </div>
</div>


@endsection


@push('page-script')
<script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>

<script type="text/javascript">
    $(function () {
        $.ajaxSetup({
          headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          }
    });

      var table = $('.data-table').DataTable({
          processing: true,
          serverSide: true,
          ajax: "",
          columns: [
              {data: 'DT_RowIndex', name: 'DT_RowIndex'},
              {data: 'name', name: 'name'},
              {data: 'email', name: 'email'},
              {data: 'role', name: 'role'},
              {data: 'action', name: 'action', orderable: false, searchable: false},
          ],
          'columnDefs': [
            {
                "targets": 0, // your case first column
                "className": "text-center",
            },
            {
                "targets": 1, // your case first column
                "className": "text-left",
            },
            {
                "targets": 2,
                "className": "text-left",
            }
 ]
      });

      $('#ajaxModel').on('shown.bs.modal', function() {
        $('input[name="name"]').focus();
    });

      $('#createNewCustomer').click(function () {
          $('#saveBtn').val("create-Customer");
          $('#Customer_id').val('');
          $('#CustomerForm').trigger("reset");
          $('#modelHeading').html("Tambah Data Pengguna");
          $('#ajaxModel').modal('show');
      });

      $('body').on('click', '.editCustomer', function () {
        var Customer_id = $(this).data('id');
        $.get("master-pengguna" +'/' + Customer_id +'/edit', function (data) {
            $('#modelHeading').html("Edit Data Unit");
            $('#saveBtn').val("edit-user");
            $('#ajaxModel').modal('show');
            $('#id').val(data.id);
            $('#name').val(data.name);
            $('#email').val(data.email);
            $('#role').val(data.role);
            $('#id_unit').val(data.id_unit);
        })
     });

      $('#saveBtn').click(function (e) {
          e.preventDefault();
          $(this).html('Sending..');

          $.ajax({
            data: $('#CustomerForm').serialize(),
            url: "",
            type: "POST",
            dataType: 'json',
            success: function (data) {
                $('#CustomerForm').trigger("reset");
                $('#ajaxModel').modal('hide');
                table.draw();
                if (data.status=='200')
                {
                    Swal.fire(
                    'Good job!',
                    data.success,
                    'success'
                );
                }else
                {
                    Swal.fire(
                    'Error !',
                    data.success,
                    'error'
                );
                }

                $('#saveBtn').html('<i class="fas fa-save"></i> Simpan');


            },
            error: function (data) {
                $('#saveBtn').html('<i class="fas fa-save"></i> Simpan');
            }
        });
      });

      $('body').on('click', '.deleteCustomer', function () {

          var Customer_id = $(this).data("id");
          Swal.fire({
                title: 'Apakah yakin akan menghapus data ?',
                showCancelButton: true,
                confirmButtonText: '<i class="fas fa-save"></i> Iya',
        }).then((result) => {
            /* Read more about isConfirmed, isDenied below */
            if (result.isConfirmed) {
                $.ajax({
                    type: "DELETE",
                    url: "master-pengguna/delete"+'/'+Customer_id,
                    success: function (data) {
                        table.draw();
                        Swal.fire(data.success, '', 'success')
                    },
                    error: function (data) {
                        console.log('Error:', data);
                    }
                });

            } else if (result.isDenied) {
                Swal.fire('Tidak Terjadi Perubahan Data', '', 'info')
            }
        })


      });

    });
  </script>

@endpush
