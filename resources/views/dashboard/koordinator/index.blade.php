@extends('layouts.master-template')
@push('page-script')
<style type="text/css">
.dataTables_filter input {
    width: 450px !important;
}
table.dataTable thead tr {
  background-color: #28a745;
  color:white
}
</style>

@endpush
@section('contents')
<div class="container">
    <h1>Kelola Data Koordinator </h1>
    
    <br/>
    <table class="table table-bordered data-table">
        <thead>
            <tr>
                <th>No</th>
                <th width="10%">ID</th>
                <th width="10%">NIK</th>
                <th width="20%" class="text-center">NAMA LENGKAP</th>
                <th width="30%" class="text-center">ALAMAT</th>
                <th width="20%" class="text-center">Whatsapp</th>
                <th width="20%">Action</th>
            </tr>
        </thead>
        <tbody>
        </tbody>
    </table>
</div>




@endsection


@push('page-script')
<script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>

<script type="text/javascript">
    $(function () {
        $.ajaxSetup({
          headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          }
    });

      var table = $('.data-table').DataTable({
          processing: true,
          serverSide: true,
          ajax: "",
          columns: [
              {data: 'DT_RowIndex', name: 'DT_RowIndex'},
              {data: 'id_masyarakat', name: 'id_masyarakat'},
              {data: 'nik', name: 'nik'},
              {data: 'nama_lengkap', name: 'nama_lengkap'},
              {data: 'alamat', name: 'alamat'},
              {data: 'nomor_wa', name: 'nomor_wa'},
              {data: 'action', name: 'action', orderable: false, searchable: false},
          ],
          'columnDefs': [
            {
                "targets": 0, // your case first column
                "className": "text-center",
            },
            {
                "targets": 1, // your case first column
                "className": "text-center",
            },
            {
                "targets": 2,
                "className": "text-left",
            },
            {
                "targets": 4, // your case first column
                "className": "text-left",
            },
 ]
      });

      $('#ajaxModel').on('shown.bs.modal', function() {
        $('input[name="kode"]').focus();
    });

      $('#createNewCustomer').click(function () {
          $('#saveBtn').val("create-Customer");
          $('#Customer_id').val('');
          $('#CustomerForm').trigger("reset");
          $('#modelHeading').html("Tambah Data Master Penerimaan");
          $('#ajaxModel').modal('show');
          $('#kode').focus();
      });

      $('body').on('click', '.editCustomer', function () {
        var Customer_id = $(this).data('id');
        $.get("" +'/' + Customer_id +'/edit', function (data) {
            $('#modelHeading').html("Edit Data Master Penerimaan");
            $('#saveBtn').val("edit-user");
            $('#ajaxModel').modal('show');
            $('#idPenerimaan').val(data.id_penerimaan);
            $('#istilah').val(data.nama_pemasukan);
            $('#penjelasan').val(data.penjelasan);
        })
     });

      $('#saveBtn').click(function (e) {
          e.preventDefault();
          $(this).html('Sending..');

          $.ajax({
            data: $('#CustomerForm').serialize(),
            url: "",
            type: "POST",
            dataType: 'json',
            success: function (data) {

                $('#CustomerForm').trigger("reset");
                $('#ajaxModel').modal('hide');
                table.draw();
                Swal.fire(
                    'Good job!',
                    data.success,
                    'success'
                );
                $('#saveBtn').html('<i class="fas fa-save"></i> Simpan');


            },
            error: function (data) {
                console.log('Error:', data);
                $('#saveBtn').html('<i class="fas fa-save"></i> Simpan');
            }
        });
      });

      $('body').on('click', '.deleteCustomer', function () {

          var Customer_id = $(this).data("id");
          Swal.fire({
                title: 'Do you want to save the changes?',
                showCancelButton: true,
                confirmButtonText: '<i class="fas fa-save"></i> Simpan',
        }).then((result) => {
            /* Read more about isConfirmed, isDenied below */
            if (result.isConfirmed) {
                $.ajax({
                    type: "DELETE",
                    url: "delete"+'/'+Customer_id,
                    success: function (data) {
                        table.draw();
                        Swal.fire(data.success, '', 'success')
                    },
                    error: function (data) {
                        console.log('Error:', data);
                    }
                });

            } else if (result.isDenied) {
                Swal.fire('Tidak Terjadi Perubahan Data', '', 'info')
            }
        })


      });

    });
  </script>

@endpush
