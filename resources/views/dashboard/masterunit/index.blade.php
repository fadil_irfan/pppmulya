@extends('layouts.master-template')
@push('page-script')
<style type="text/css">
.dataTables_filter input {
    width: 450px !important;
}
table.dataTable thead tr {
  background-color: #28a745;
  color:white
}
</style>

@endpush

@section('contents')
<div class="container">
    <h1>Master Unit </h1>
    <a class="btn btn-success" href="javascript:void(0)" id="createNewCustomer"><i class="fas fa-save"></i> Tambah Data</a>
    <br/>
    <br/>
    <table class="table table-bordered data-table">
        <thead>
            <tr>
                <th>No</th>
                <th width="10%">Kode</th>
                <th width="70%" class="text-center">Nama Unit</th>
                <th width="20%">Action</th>
            </tr>
        </thead>
        <tbody>
        </tbody>
    </table>
</div>

<div class="modal fade" id="ajaxModel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="modelHeading"></h4>
            </div>
            <div class="modal-body">
                <form id="CustomerForm" name="CustomerForm" class="form-horizontal" enctype="multipart/form-data" method="POST">
                   <input type="hidden" name="idUnit" id="idUnit">
                   <div class="row">
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label for="name" class="col-sm-6 control-label">Kode Unit</label>
                            <div class="col-sm-12">
                                <input type="text" class="form-control" id="kode_unit" name="kode_unit" placeholder="Masukan Kode Unit" value="" maxlength="50" required autofocus>
                            </div>
                        </div>

                    </div>
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label for="name" class="col-sm-6 control-label">Nama Unit</label>
                            <div class="col-sm-12">
                                <input type="text" class="form-control" id="nama_unit" name="nama_unit" placeholder="Masukan Nama Unit" value="" required="" >
                            </div>
                        </div>

                    </div>
                   </div>




                    <div class="form-group">
                        <label for="name" class="col-sm-6 control-label">Alamat Unit</label>
                        <div class="col-sm-12">
                            <textarea id="alamat_unit" name="alamat_unit" required="" placeholder="Masukan Alamat Unit" class="form-control"></textarea>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="name" class="col-sm-12 control-label">Jabatan Tanda Tangan I </label>
                                <div class="col-sm-12">
                                    <input type="text" class="form-control" id="jabatan_tanda_wadek" name="jabatan_tanda_wadek" placeholder="Masukan Jabatan Tanda Tangan I" value="" required="" >
                                </div>
                            </div>

                        </div>

                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="name" class="col-sm-12 control-label">Nama Tanda Tangan I </label>
                                <div class="col-sm-12">
                                    <input type="text" class="form-control" id="nama_tanda_wadek" name="nama_tanda_wadek" placeholder="Masukan Nama Tanda Tangan I" value="" required="" >
                                </div>
                            </div>

                        </div>

                    </div>

                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="name" class="col-sm-12 control-label">Jabatan Tanda Tangan II </label>
                                <div class="col-sm-12">
                                    <input type="text" class="form-control" id="jabatan_tanda_kepala" name="jabatan_tanda_kepala" placeholder="Masukan Jabatan Tanda Tangan II" value="" required="">
                                </div>
                            </div>

                        </div>

                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="name" class="col-sm-12 control-label">Nama Tanda Tangan II </label>
                                <div class="col-sm-12">
                                    <input type="text" class="form-control" id="nama_tanda_kepala" name="nama_tanda_kepala" placeholder="Masukan Nama Tanda Tangan II" value="" required="">
                                </div>
                            </div>

                        </div>

                    </div>


                    <div class="form-group">
                        <label for="name" class="col-sm-12 control-label">Upload Kop Unit (Width x Height) (800x300) </label>
                        <div class="col-sm-12">
                            <div class="needsclick dropzone" id="document-dropzone">

                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div id="preview">

                            </div>

                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-9">

                        </div>
                        <div class="col-sm-3">
                            <div class="col-sm-offset-2 col-sm-12 ml-4">
                                <button type="submit" class="btn btn-primary" id="saveBtn" value="create"><i class="fas fa-save"></i> Simpan
                                </button>
                               </div>

                        </div>
                    </div>


                </form>


            </div>
        </div>
    </div>
</div>


@endsection


@push('page-script')
<script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>

<script type="text/javascript">
    $(function () {
        $.ajaxSetup({
          headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          }
    });

      var table = $('.data-table').DataTable({
          processing: true,
          serverSide: true,
          ajax: "",
          columns: [
              {data: 'DT_RowIndex', name: 'DT_RowIndex'},
              {data: 'kode_unit', name: 'kode_unit'},
              {data: 'nama_unit', name: 'nama_unit'},
              {data: 'action', name: 'action', orderable: false, searchable: false},
          ],
          'columnDefs': [
            {
                "targets": 0, // your case first column
                "className": "text-center",
            },
            {
                "targets": 1, // your case first column
                "className": "text-center",
            },
            {
                "targets": 2,
                "className": "text-left",
            }
 ]
      });

      $('#ajaxModel').on('shown.bs.modal', function() {
        $('input[name="kode_unit"]').focus();
    });

      $('#createNewCustomer').click(function () {
          $('#saveBtn').val("create-Customer");
          $('#Customer_id').val('');
          $('#CustomerForm').trigger("reset");
          $('#modelHeading').html("Tambah Data Unit");
          $('#ajaxModel').modal('show');
      });

      $('body').on('click', '.editCustomer', function () {
        var Customer_id = $(this).data('id');
        $.get("master-unit" +'/' + Customer_id +'/edit', function (data) {
            $('#modelHeading').html("Edit Data Unit");
            $('#saveBtn').val("edit-user");
            $('#ajaxModel').modal('show');
            $('#idUnit').val(data.id_unit);
            $('#kode_unit').val(data.kode_unit);
            $('#nama_unit').val(data.nama_unit);
            $('#alamat_unit').val(data.alamat_unit);
            $('#jabatan_tanda_wadek').val(data.jabatan_tanda_wadek);
            $('#nama_tanda_wadek').val(data.nama_tanda_wadek);
            $('#jabatan_tanda_kepala').val(data.jabatan_tanda_kepala);
            $('#nama_tanda_kepala').val(data.nama_tanda_kepala);
            $('#preview').append("<img src='files/kop/"+data.kop_unit+"' width='100%' height='300' style='display: inline-block;'>");
        })
     });

      $('#saveBtn').click(function (e) {
          e.preventDefault();
          $(this).html('Sending..');

          $.ajax({
            data: $('#CustomerForm').serialize(),
            url: "",
            type: "POST",
            dataType: 'json',
            success: function (data) {
                $('#CustomerForm').trigger("reset");
                $('#ajaxModel').modal('hide');
                table.draw();
                if (data.status=='200')
                {
                    Swal.fire(
                    'Good job!',
                    data.success,
                    'success'
                );
                }else
                {
                    Swal.fire(
                    'Error !',
                    data.success,
                    'error'
                );
                }

                $('#saveBtn').html('<i class="fas fa-save"></i> Simpan');


            },
            error: function (data) {
                $('#saveBtn').html('<i class="fas fa-save"></i> Simpan');
            }
        });
      });




      $('body').on('click', '.deleteCustomer', function () {

          var Customer_id = $(this).data("id");
          Swal.fire({
                title: 'Apakah yakin akan menghapus data ?',
                showCancelButton: true,
                confirmButtonText: '<i class="fas fa-save"></i> Iya',
        }).then((result) => {
            /* Read more about isConfirmed, isDenied below */
            if (result.isConfirmed) {
                $.ajax({
                    type: "DELETE",
                    url: "master-unit/delete"+'/'+Customer_id,
                    success: function (data) {
                        table.draw();
                        Swal.fire(data.success, '', 'success')
                    },
                    error: function (data) {
                        console.log('Error:', data);
                    }
                });

            } else if (result.isDenied) {
                Swal.fire('Tidak Terjadi Perubahan Data', '', 'info')
            }
        })


      });

    });
  </script>

   <!-- Script Dropzone -->
   <script>
    var uploadedDocumentMap = {}
    Dropzone.options.documentDropzone = {
       url: '{{ route('storeMedia') }}',
       maxFilesize: 2, // MB
       addRemoveLinks: true,
       acceptedFiles: ".jpeg,.jpg,.png,.gif",
       headers: {
          'X-CSRF-TOKEN': "{{ csrf_token() }}"
       },
       success: function(file, response) {
          $('form').append('<input type="hidden" name="photo[]" value="' + response.name + '">')
          uploadedDocumentMap[file.name] = response.name
       },
       removedfile: function(file) {
          file.previewElement.remove()
          var name = ''
          if (typeof file.file_name !== 'undefined') {
             name = file.file_name
          } else {
             name = uploadedDocumentMap[file.name]
          }
          $('form').find('input[name="photo[]"][value="' + name + '"]').remove()
       }
    }
 </script>


@endpush
