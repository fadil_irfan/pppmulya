<!DOCTYPE html>
<html>
<head>
    @include('layouts.head')
    @stack('page-stylesheet')

</head>


<body>
    <div class="wrapper">
        <!-- Sidebar  -->
<nav id="sidebar">
    <div class="sidebar-header">
        <h3>HAJI YUYUN HIDAYAT</h3>
    </div>

    <ul class="list-unstyled components">
        <img class="rounded mx-auto d-block" width="100%" src="{{ url('images/logo.png') }}" width="50%" height="50%"  class="img-thumbnail">
        
        <li class="{{ (request()->is('dashboard*')) ? 'active' : '' }}">
            <a href="{{ url('dashboard-koordinator') }}">
                <i class="fas fa-home"></i>
                Beranda
            </a>
        </li>
        <li>
            <a href="#pageSubmenu" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">
                <i class="fas fa-copy"></i>
                Data 
            </a>
            <ul class="{{ (request()->is('masyarakat*')) ? '' : 'collapse' }} list-unstyled" id="pageSubmenu">
                <li class="{{ (request()->is('masyarakat-koordinator')) ? 'active' : '' }}">
                    <a href="{{ url('masyarakat-koordinator') }}"><i class="fas fa-street-view"></i> Kelola Masyarakat</a>
                </li>

            </ul>


        </li>

        <li>
            <a href="#pageSubmenuRelawan" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">
                <i class="fas fa-users"></i>
                Rekap Relawan 
            </a>
            <ul class="{{ (request()->is('rekap*')) ? '' : 'collapse' }} list-unstyled" id="pageSubmenuRelawan">
                <li class="{{ (request()->is('rekapmasyarakat')) ? 'active' : '' }}">
                    <a href="{{ url('rekapmasyarakat') }}"><i class="fas fa-street-view"></i> Masyarakat</a>
                </li>
               
            </ul>


        </li>        
    </ul>

    <ul class="list-unstyled CTAs">
        <li>
            <a href="{{ route('logout') }}" style="cursor: pointer" onclick="event.preventDefault();
            document.getElementById('logout-form').submit();" class="download"><i class="fas fa-sign-out-alt"></i> Keluar</a>
            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                @csrf
            </form>
        </li>

    </ul>
</nav>

        <!-- Page Content  -->
        <div id="content">
            <nav class="navbar navbar-expand-lg navbar-light bg-light">
                <div class="container-fluid">

                    <button type="button" id="sidebarCollapse" class="btn btn-info">
                        <i class="fas fa-align-left"></i>
                        <span>Toggle Sidebar</span>
                    </button>
                    <button class="btn btn-dark d-inline-block d-lg-none ml-auto" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                        <i class="fas fa-align-justify"></i>
                    </button>


                </div>
            </nav>
            @yield('contents')

        </div>

    </div>

    @include('layouts.footer')


    @include('layouts.script')
    @stack('page-script')


</body>

</html>
