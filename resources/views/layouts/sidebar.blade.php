<!-- Sidebar  -->
<nav id="sidebar">
    <div class="sidebar-header">
        <h3>HAJI YUYUN HIDAYAT</h3>
    </div>

    <ul class="list-unstyled components">
        <img class="rounded mx-auto d-block" width="100%" src="{{ url('images/logo.png') }}" width="50%" height="50%"  class="img-thumbnail">
        
        <li class="{{ (request()->is('dashboard*')) ? 'active' : '' }}">
            <a href="{{ url('dashboard') }}">
                <i class="fas fa-home"></i>
                Beranda
            </a>
        </li>
        <li>
            <a href="#pageSubmenu" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">
                <i class="fas fa-copy"></i>
                Data 
            </a>
            <ul class="{{ (request()->is('data*')) ? '' : 'collapse' }} list-unstyled" id="pageSubmenu">
                <li class="{{ (request()->is('data-koordinator')) ? 'active' : '' }}">
                    <a href="{{ url('data-koordinator') }}"><i class="fas fa-user-tie"></i> Kelola Koordinator</a>
                </li>
                <li class="{{ (request()->is('data-masyarakat')) ? 'active' : '' }}">
                    <a href="{{ url('data-masyarakat') }}"><i class="fas fa-street-view"></i> Kelola Masyarakat</a>
                </li>
                <li class="{{ (request()->is('data-admin')) ? 'active' : '' }}">
                    <a href="{{ url('data-admin') }}"><i class="fas fa-university"></i> Kelola Admin</a>
                </li>
                

            </ul>


        </li>
        <li class="{{ (request()->is('aspirasi*')) ? 'active' : '' }}">
            <a href="{{ url('aspirasi') }}">
                <i class="fas fa-envelope"></i>
                Aspirasi
            </a>
        </li>

        <li>
            <a href="#pageSubmenuRelawan" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">
                <i class="fas fa-users"></i>
                Rekap Relawan 
            </a>
            <ul class="{{ (request()->is('rekap*')) ? '' : 'collapse' }} list-unstyled" id="pageSubmenuRelawan">
                <li class="{{ (request()->is('rekap-koordinator')) ? 'active' : '' }}">
                    <a href="{{ url('rekap-koordinator') }}"><i class="fas fa-user-tie"></i>Koordinator</a>
                </li>
                <li class="{{ (request()->is('rekap-masyarakat')) ? 'active' : '' }}">
                    <a href="{{ url('rekap-masyarakat') }}"><i class="fas fa-street-view"></i> Masyarakat</a>
                </li>
               
            </ul>


        </li>        
    </ul>

    <ul class="list-unstyled CTAs">
        <li>
            <a href="{{ route('logout') }}" style="cursor: pointer" onclick="event.preventDefault();
            document.getElementById('logout-form').submit();" class="download"><i class="fas fa-sign-out-alt"></i> Keluar</a>
            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                @csrf
            </form>
        </li>

    </ul>
</nav>
