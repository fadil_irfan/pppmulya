<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="shortcut icon" href="{{ url('images/logo.png') }}" type="image/x-icon">
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
    <link href="https://fonts.googleapis.com/css2?family=Quicksand:wght@300;400;500;600&display=swap" rel="stylesheet">
     <!-- Font Awesome JS -->
     <script defer src="https://use.fontawesome.com/releases/v5.0.13/js/solid.js" integrity="sha384-tzzSw1/Vo+0N5UhStP3bvwWPq+uvzCMfrN1fEFe+xBmv1C/AtVX5K0uZtmcHitFZ" crossorigin="anonymous"></script>
     <script defer src="https://use.fontawesome.com/releases/v5.0.13/js/fontawesome.js" integrity="sha384-6OIrr52G08NpOFSZdxxz1xdNSndlD4vdcf/q2myIUVO0VsqaGHJsB0RaBE01VTOY" crossorigin="anonymous"></script>
    <title>Relawan Haji Yuyun Hidayat</title>
    <style>
        body,html {
            font-family: 'Quicksand', sans-serif;
            /*background: url("/background-fo.png") no-repeat fixed;*/
            background-repeat: no-repeat;
            background-size: 100% 100%;
            background-color: forestgreen;
            width: 100%;
            height: 100%;
   
        }
        

        /*untuk layar device berukuran kecil*/
@media screen and (min-width: 450px) {
  #logo{
            margin-top:50%;
            text-align: center;
            margin-bottom: 5%;

        }
}
 
/*untuk layar device berukuran sedang*/
@media screen and (min-width: 550px) {
  #logo{
            margin-top:50%;
            text-align: center;
            margin-bottom: 5%;

        }
}
 
/*untuk layar device berukuran besar*/
@media screen and (min-width: 800px) {
  #logo{
            margin-top:50%;
            text-align: center;
            margin-bottom: 5%;

        }
}


    </style>

  </head>
  <body>
    
    
        <div class="row justify-content-center align-items-center h-100">
                
                @yield('content')
        </div>


    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/jquery@3.5.1/dist/jquery.min.js">
</script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
    @stack('page-script')
  </body>
</html>
