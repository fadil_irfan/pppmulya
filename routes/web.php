<?php

use App\Http\Controllers\FrontController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\MasterKoordinatorController;
use App\Http\Controllers\MasterMasyarakatController;

use App\Http\Controllers\MasterPenggunaController;
use App\Http\Controllers\MasyarakatController;
use App\Http\Controllers\RekapKoordinatorController;
use App\Http\Controllers\WaController;
use Illuminate\Support\Facades\Route;





/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/* Route::get('/', function () {
    return redirect('/login');
}); */
Route::get('/', [FrontController::class, 'index']);
Route::get('login-koordinator', [FrontController::class, 'login'])->name('login-koordinator');

Route::post('cek-koordinator', [FrontController::class, 'cekKoordinator'])->name('cek-koordinator');

Route::post('simpan-aspirasi', [FrontController::class, 'storeAspirasi']);
Route::get('/reload-captcha', [FrontController::class, 'reloadCaptcha']);


Route::get('register', [MasyarakatController::class, 'register']);

Route::get('ref-wilayah-kabupaten', [MasyarakatController::class, 'wilayahKabupaten']);
Route::get('ref-wilayah-kecamatan', [MasyarakatController::class, 'wilayahKelurahan']);
Route::post('simpan-masyarakat', [MasyarakatController::class, 'storeMasyarakat']);


/*

Route::get('/home', function () {
    return view('home');
}); */

Route::get('dashboard', [HomeController::class, 'index']);
Route::get('aspirasi', [HomeController::class, 'aspirasi']);
Route::get('keluar', [HomeController::class, 'keluar']);

Route::get('send-wa', [WaController::class, 'sendWa']);

    Route::get('data-koordinator', [MasterKoordinatorController::class, 'index']);
    Route::get('dashboard-koordinator', [MasterKoordinatorController::class, 'dashboard']);

    Route::get('masyarakat-koordinator', [MasterKoordinatorController::class, 'masyarakat']);

    Route::get('tambah-koordinator', [MasterKoordinatorController::class, 'tambahmasyarakat']);
    Route::post('simpan-koordinator', [MasterKoordinatorController::class, 'storeKoordinator']);

    Route::get('rekapmasyarakat', [MasterKoordinatorController::class, 'rekapmasyarakat']);

    Route::post('data-koordinator', [MasterKoordinatorController::class, 'store']);
    Route::delete('/delete/{id}', [MasterKoordinatorController::class, 'destroy']);

    Route::get('data-masyarakat', [MasterMasyarakatController::class, 'index']);
    Route::post('data-masyarakat', [MasterMasyarakatController::class, 'store']);
    Route::get('/data-masyarakat/{id}/edit', [MasterMasyarakatController::class, 'edit']);
    Route::delete('/data-masyarakat/delete/{id}', [MasterMasyarakatController::class, 'destroy']);

    

    Route::get('data-admin', [MasterPenggunaController::class, 'index']);
    Route::post('data-admin', [MasterPenggunaController::class, 'store']);
    Route::get('/data-admin/{id}/edit', [MasterPenggunaController::class, 'edit']);
    Route::delete('/data-admin/delete/{id}', [MasterPenggunaController::class, 'destroy']);

    Route::get('rekap-koordinator', [RekapKoordinatorController::class, 'index']);

    Route::get('rekap-masyarakat', [RekapKoordinatorController::class, 'masyarakat']);


